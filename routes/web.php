<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/test', 'HomeController@test')->name('test');


/**Servicios de panel de control**/
Route::get('/panel/panel', 'PanelController@panel')->name('panel.panel');
Route::get('/panel/correos', 'PanelController@correos')->name('panel.correos');

/**servicios de usuario**/
Route::get('/usuarios/lista', 'Catalogos\UsuariosController@index')->name('usuarios.lista');
Route::get('/usuarios/edit/{id}', 'Catalogos\UsuariosController@edit')->name('usuarios.edit')->where('id', '[0-9]+');
Route::get('/usuarios/addemp/{id}', 'Catalogos\UsuariosController@addemp')->name('usuarios.addemp')->where('id', '[0-9]+');
Route::get('/usuarios/addsuc/{id}', 'Catalogos\UsuariosController@addsuc')->name('usuarios.addsuc')->where('id', '[0-9]+');
Route::post('updateUser',  'Catalogos\UsuariosController@saveUsuario')->name('updateUser');
Route::post('updateUsrFam',  'Catalogos\UsuariosController@updateUsrFam')->name('updateUsrFam');
Route::post('activarUsu',  'Catalogos\UsuariosController@activarUsu')->name('activarUsu');
Route::post('asigEmp',  'Catalogos\UsuariosController@asigEmp')->name('asigEmp');
Route::post('asigSuc',  'Catalogos\UsuariosController@asigSuc')->name('asigSuc');
Route::post('quitAll',  'Catalogos\UsuariosController@quitAll')->name('quitAll');
Route::post('quitAllSuc',  'Catalogos\UsuariosController@quitAllSuc')->name('quitAllSuc');
Route::post('addFiltroEmp',  'Catalogos\UsuariosController@addFiltroEmp')->name('addFiltroEmp');
Route::post('addFiltroSuc',  'Catalogos\UsuariosController@addFiltroSuc')->name('addFiltroSuc');

/**servicios de razones**/
Route::get('/razones/lista', 'Catalogos\RazonesController@index')->name('razones.lista');
/**servicios de plazas**/
Route::get('/plazas/lista', 'Catalogos\PlazasController@index')->name('plazas.lista');
/**servicios de zonas**/
Route::get('/zonas/lista', 'Catalogos\ZonasController@index')->name('zonas.lista');
/**servicios de sucursales**/
Route::get('/sucursales/lista', 'Catalogos\SucursalesController@index')->name('sucursales.lista');
/**servicios de empleados**/
Route::get('/empleados/lista', 'Catalogos\EmpleadosController@index')->name('empleados.lista');
/**servicios de puestos**/
Route::get('/puestos/lista', 'Catalogos\PuestosController@index')->name('puestos.lista');
/**servicios de checklist**/
Route::get('/checklist/lista', 'Catalogos\ChecklistController@index')->name('checklist.lista');
/**servicios de cuestionarios**/
Route::get('/cuestionarios/lista', 'Catalogos\CuestionariosController@index')->name('cuestionarios.lista');

/**Servicios de actividades de usuario**/
Route::get('/actividades/lista', 'Supervision\ActividadesController@index')->name('actividades.lista');
Route::post('getActividadFiltro',  'Supervision\ActividadesController@getActividadFiltro')->name('getActividadFiltro');


/**Request globales**/
Route::post('getPlazas',  'GlobalController@getPlazas')->name('getPlazas');
Route::post('getTiendas',  'GlobalController@getTiendas')->name('getTiendas');
Route::post('getFiltroEmp',  'GlobalController@getFiltroEmp')->name('getFiltroEmp');
Route::post('getFiltroSuc',  'GlobalController@getFiltroSuc')->name('getFiltroSuc');
