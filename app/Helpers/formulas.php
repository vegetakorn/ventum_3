<?php
//app/Helpers/Envato/User.php
namespace App\Helpers;

use DB;

class Formulas {


    /**
     * @param int $user_id User-id
     *
     * @return string
     */
    public static function testFormula($id) {


        return "El id recibido es ". $id;
    }

    public static function apiKeyMap()
    {
        $api = "AIzaSyBJdxJiIdSuwiDfeKebLbFBw4bwea9SUYk";

        return $api;
    }

    public static function getArrMeses( ) {

        $mes = date('Y-m-d');

        $arrMeses = array();

        for($i = 12 ; $i >= 0; $i--)
        {
            $arrMeses[] =  date("m-Y",strtotime($mes."- $i month"));
        }

        return $arrMeses;
    }


    public static function avancePresupuesto( $ventas, $presupuesto) {

        if($presupuesto != 0)
        {
            $avance = round($ventas / $presupuesto * 100, 2);
        }else
        {
            $avance = 0;
        }

        return $avance;
    }

    public static function avancePresupuestoTeorico( $diasPresTot, $totalSuc) {

        $dias = date('d') -1;
        if($dias == 0)
        {
            $dias = 1;
        }

        $diasPresProm = round($diasPresTot/$totalSuc, 2);

        if($diasPresProm == 0)
        {
            $avance = 0;
        }else
        {
            $avance = round($dias / $diasPresProm * 100, 2);
        }



        return $avance;
    }

    public static function tendencia( $ventas, $diaspres) {

        //obtenemos la meta diaria real
        $dias = date('d') -1;
        if($dias == 0)
        {
            $dias = 1;
        }
        $metaDiaReal = round($ventas / $dias, 4);
        $tendencia = round($metaDiaReal * $diaspres ,2);
        return $tendencia;
    }

    public static function tendenciaPorcentaje( $tendencia, $presupuesto) {

      if($presupuesto != 0)
      {
          $tendenciaPor = round($tendencia / $presupuesto * 100 ,2);
      }else
      {
          $tendenciaPor = 0;
      }

        return $tendenciaPor;
    }
    public static function transDiaPromedio( $tickets, $registros) {

       if($registros != 0)
       {
           $transProm = round($tickets / $registros,2);
       }else
       {
           $transProm = 0;
       }

        return $transProm;

    }

    public static function artXticketPromedio( $noartxtck, $registros) {

        if($registros != 0)
        {
            $notckProm = round($noartxtck / $registros,2);
        }else
        {
            $notckProm = 0;
        }
        return $notckProm;

    }
    public static function ticketPromedio( $ventas, $tickets) {

        if($tickets != 0)
        {
            $tckProm = round($ventas / $tickets ,2);
        }else
        {
            $tckProm = 0;
        }

        return $tckProm;

    }

    public static function metaDiaria( $presupuesto, $diaspres) {

        if($diaspres != 0)
        {
            $meta = round($presupuesto / $diaspres ,2);
        }else
        {
            $meta = 0;
        }

        return $meta;

    }

    public static function diferenciaDias($fecha1, $fecha2)
    {
        $fecha1 = explode("-", $fecha1);
        $fecha2 = explode("-", $fecha2);
        $fechaMk1=mktime(0,0,0,$fecha1[1],$fecha1[2],$fecha1[0]);
        $fechaMk2=mktime(0,0,0,$fecha2[1],$fecha2[2],$fecha2[0]);

        $segundos_diferencia=$fechaMk1-$fechaMk2;

        //convierto segundos en días
        $dias_diferencia = $segundos_diferencia / (60 * 60 * 24);

        //obtengo el valor absoulto de los días (quito el posible signo negativo)
        $dias_diferencia = abs($dias_diferencia);

        //quito los decimales a los días de diferencia
        $dias_diferencia = floor($dias_diferencia);

        return $dias_diferencia;
    }

    public static function diferenciaSegundos($fecha1, $fecha2)
    {
        $fecha1Mes  = date('m', strtotime($fecha1));
        $fecha1Anio = date('Y', strtotime($fecha1));
        $fecha1Dia  = date('d', strtotime($fecha1));
        $fecha1Hora = date('H', strtotime($fecha1));
        $fecha1Min  = date('i', strtotime($fecha1));
        $fecha1Seg  = date('s', strtotime($fecha1));

        $fecha2Mes  = date('m', strtotime($fecha2));
        $fecha2Anio = date('Y', strtotime($fecha2));
        $fecha2Dia  = date('d', strtotime($fecha2));
        $fecha2Hora = date('H', strtotime($fecha2));
        $fecha2Min  = date('i', strtotime($fecha2));
        $fecha2Seg  = date('s', strtotime($fecha2));


        $fechaMk1=mktime($fecha1Hora,$fecha1Min,$fecha1Seg,$fecha1Mes,$fecha1Dia,$fecha1Anio);
        $fechaMk2=mktime($fecha2Hora,$fecha2Min,$fecha2Seg,$fecha2Mes,$fecha2Dia,$fecha2Anio);

        $segundos_diferencia=$fechaMk1-$fechaMk2;

        return $segundos_diferencia * -1;
    }

    public static function fechaBD($fecha1)
    {
        $fecha = date('Y-m-d', strtotime($fecha1));

        return $fecha;

    }
    /**Devuelve los dias que tiene un mes**/
    public static function diasMes($mes, $anio)
    {

        //$numero = cal_days_in_month(CAL_GREGORIAN, $mes, $anio); // 31
        $numero = date('t', mktime(0, 0, 0, $mes, 1, $anio));
        return $numero;

    }


    public static function coloresSignos($actual, $anterior)
    {
       if($actual > $anterior)
       {
           $color_texto = 'text-green';
       }else if($actual ==  $anterior)
       {
           $color_texto = 'text-yellow';
       }else if($actual <  $anterior)
       {
           $color_texto = 'text-red';
       }

        return $color_texto ;
    }

    public static function flechasSignos($actual, $anterior)
    {
        if($actual > $anterior)
        {
            $flecha = 'fa-caret-up';
        }else if($actual ==  $anterior)
        {
            $flecha = 'fa-caret-left';
        }else if($actual <  $anterior)
        {
            $flecha = 'fa-caret-down';
        }

        return $flecha ;
    }

    public static function sumaVtaPres($userId, $mes, $anio)
    {
        if($mes  == 0)
        {
            $mes = date('m') ;
        }


        //buscamos las ventas, presupuestos del mes actual de la tienda
        $sql  = DB::table('ventas_mes')
                ->leftjoin('tiendas', function ($join) {
                    $join->on('tiendas.Id', '=', 'ventas_mes.tiendas_Id');
                })
                ->leftjoin('users_tiendas', function ($join) {
                    $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
                });

        $sql->where('ventas_mes.mes', "=", (int) $mes);
        $sql->where('users_tiendas.users_Id', "=", $userId);
        $sql->where('ventas_mes.anio', "=", $anio);
        $venta_mes = $sql->get();
        $query =  $sql->toSql();
        $info = collect($venta_mes)->toArray();
        $sumVta = 0;
        $sumPres = 0;
        $arrPres = array();
        for($i = 0; $i < count($info); $i++)
        {
            $arrPres[] = array("sumpres"=> $sumPres);
            $sumVta = $sumVta + $info[$i]->ventas;
            $sumPres = $sumPres + $info[$i]->presupuesto;

        }

        $arrRes = array($sumVta, $sumPres, $userId, $mes, $anio, $arrPres );
        return $arrRes;
    }

    public static function registroCampo($gentodo, $campo_id, $visita_id, $comentario, $valor, $foto, $foto64, $status)
    {
        $ruta = '/home/ventum/public_html/uploads/FotoTodo/';
        $noaplica = 0;
        if($gentodo == 3)
        {
            $noaplica = 1;
        }

        $foto_nombre = "NULL";
        //checamos si tiene foto para cargarse
        if($foto64 != "")
        {
            $foto_nombre = date('Ymdhis')."_".$foto.".png";
            //generamos la foto y la guardamos
            $b64 = $foto64;

            // Obtain the original content (usually binary data)
            $bin = base64_decode($b64);
            // Load GD resource from binary data
            $im = imageCreateFromString($bin);
            // Make sure that the GD library was able to load the image
            // This is important, because you should not miss corrupted or unsupported images
            if (!$im) {
                die('Base64 value is not a valid image');
            }
            // Specify the location where you want to save the image
            $img_file = $ruta.$foto_nombre;
            // Save the GD resource as PNG in the best possible quality (no compression)
            // This will strip any metadata or invalid contents (including, the PHP backdoor)
            // To block any possible exploits, consider increasing the compression level
            imagepng($im, $img_file, 0);
        }

        /**Buscamos en visitas_campos y en visitas_todo**/
        $campo_notodo =   DB::table('visitas_campos')
            ->where('visitas_campos.campos_Id','=',$campo_id)
            ->where('visitas_campos.visitas_Id','=',$visita_id)
            ->count();
        $campo_sitodo =   DB::table('visitas_todo')
            ->where('visitas_todo.campos_Id','=',$campo_id)
            ->where('visitas_todo.visitas_Id','=',$visita_id)
            ->count();

        //condicionales
        if($campo_notodo == 1 &&  $campo_sitodo == 1)
        {

            if($gentodo == 1)
            {


                //borrar registro en visitas_campos, actualizamos en visitas_todo
                DB::table('visitas_campos')->where('campos_Id', '=', $campo_id)->where('visitas_campos.visitas_Id','=',$visita_id)->delete();

                DB::table('visitas_todo')
                    ->where('campos_Id', $campo_id)
                    ->where('visitas_todo.visitas_Id','=',$visita_id)
                    ->update(['valor' => $valor,
                        'fecha_inicio' => date('Y-m-d'),
                        'descripcion' => $comentario,
                        'ImagenIni' => $foto_nombre,
                        'Status' => $status]);
            }else
            {

                //borrar registro en visitas_todo, actualizamos en visitas_camopos
                DB::table('visitas_todo')->where('campos_Id', '=', $campo_id)->where('visitas_todo.visitas_Id','=',$visita_id)->delete();

                DB::table('visitas_campos')
                    ->where('campos_Id', $campo_id)
                    ->where('visitas_campos.visitas_Id','=',$visita_id)
                    ->update(['valor' => $valor,
                            'comentario' => $comentario,
                            'no_aplica' => $noaplica,
                        ]
                    );

            }


        }else if ($campo_notodo == 1 &&  $campo_sitodo == 0)
        {
            if($gentodo == 1)
            {
                //borrar registro en visitas_campos, actualizamos en visitas_todo
                DB::table('visitas_campos')->where('campos_Id', '=', $campo_id)->where('visitas_campos.visitas_Id','=',$visita_id)->delete();

                DB::table('visitas_todo')->insert(
                    [
                        'visitas_Id' => $visita_id,
                        'campos_Id' => $campo_id,
                        'valor' => $valor,
                        'fecha_inicio' => date('Y-m-d'),
                        'descripcion' => $comentario,
                        'ImagenIni' => $foto_nombre,
                        'Status' => $status,

                    ]
                );

            }else
            {

                DB::table('visitas_campos')
                    ->where('campos_Id', $campo_id)
                    ->where('visitas_campos.visitas_Id','=',$visita_id)
                    ->update(['valor' => $valor,
                        'comentario' => $comentario,
                        'no_aplica' => $noaplica,

                    ]);

            }


        }else if ($campo_notodo == 0 &&  $campo_sitodo == 1)
        {
            if($gentodo == 1)
            {

                DB::table('visitas_todo')
                    ->where('campos_Id', $campo_id)
                    ->where('visitas_todo.visitas_Id','=',$visita_id)
                    ->update(['valor' => $valor,
                        'fecha_inicio' => date('Y-m-d'),
                        'descripcion' => $comentario,
                        'ImagenIni' => $foto_nombre,
                        'Status' => $status]);
            }else
            {
                //borrar registro en visitas_todo, actualizamos en visitas_camopos
                DB::table('visitas_todo')->where('campos_Id', '=', $campo_id)->where('visitas_todo.visitas_Id','=',$visita_id)->delete();


                DB::table('visitas_campos')->insert(
                    [
                        'visitas_Id' => $visita_id,
                        'campos_Id' => $campo_id,
                        'no_aplica' => $noaplica,
                        'valor' => $valor,
                        'comentario' => $comentario

                    ]
                );


            }

        }else if ($campo_notodo == 0 &&  $campo_sitodo == 0)
        {
            if($gentodo == 1)
            {

                DB::table('visitas_todo')->insert(
                    [
                        'visitas_Id' => $visita_id,
                        'campos_Id' => $campo_id,
                        'valor' => $valor,
                        'fecha_inicio' => date('Y-m-d'),
                        'descripcion' => $comentario,
                        'ImagenIni' => $foto_nombre,
                        'Status' => $status,

                    ]
                );

            }else
            {
                DB::table('visitas_campos')->insert(
                    [
                        'visitas_Id' => $visita_id,
                        'campos_Id' => $campo_id,
                        'valor' => $valor,
                        'no_aplica' => $noaplica,
                        'comentario' => $comentario

                    ]
                );
            }
        }

        return "ok";
    }

}