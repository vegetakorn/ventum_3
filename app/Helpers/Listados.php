<?php
//app/Helpers/Envato/User.php
namespace App\Helpers;

use DB;

class Listados {
    /**
     * @param int $user_id User-id
     *
     * @return string
     */
    public static function listaTiendas($user_Id, $type) {

        if($type != 0)
        {
            $sql  = DB::table('users_tiendas')
                ->leftjoin('tiendas', function ($join) {
                    $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
                })
                ->leftjoin('razon_social', function ($join) {
                    $join->on('tiendas.razon_Id', '=', 'razon_social.Id');
                })
                ->leftjoin('plazas', function ($join) {
                    $join->on('tiendas.plaza_Id', '=', 'plazas.Id');
                })
                ->leftjoin('zonas', function ($join) {
                    $join->on('tiendas.zona', '=', 'zonas.Id');
                })
                ->select('tiendas.*', 'users_tiendas.Id as usti_id', 'razon_social.nombre as razon', 'plazas.plaza', 'zonas.zona' )
                ->where('users_tiendas.users_Id', "=", $user_Id)
                ->where('tiendas.activo', "=", 1)
                ->get();
        }else
        {
            $sql  = DB::table('tiendas')
                ->leftjoin('razon_social', function ($join) {
                    $join->on('tiendas.razon_Id', '=', 'razon_social.Id');
                })
                ->leftjoin('plazas', function ($join) {
                    $join->on('tiendas.plaza_Id', '=', 'plazas.Id');
                })
                ->leftjoin('zonas', function ($join) {
                    $join->on('tiendas.zona', '=', 'zonas.Id');
                })
                ->select('tiendas.*', 'razon_social.nombre as razon', 'plazas.plaza', 'zonas.zona' )
                ->where('tiendas.activo', "=", 1)
                ->get();
        }


        return $sql;
    }

    public static function listaEmpleados($user_Id, $type) {

        if($type != 0)
        {
            $sql  = DB::table('users_empleados')
                ->leftjoin('empleados', function ($join) {
                    $join->on('empleados.Id', '=', 'users_empleados.empleados_Id');
                })
                ->leftjoin('razon_social', function ($join) {
                    $join->on('empleados.razon_Id', '=', 'razon_social.Id');
                })
                ->leftjoin('plazas', function ($join) {
                    $join->on('empleados.plaza_Id', '=', 'plazas.Id');
                })
                ->leftjoin('zonas', function ($join) {
                    $join->on('empleados.zona_Id', '=', 'zonas.Id');
                })
                ->leftjoin('puestos', function ($join) {
                    $join->on('empleados.puesto_Id', '=', 'puestos.Id');
                })
                ->select('empleados.*', 'users_empleados.Id as usti_id','puestos.puesto', 'razon_social.nombre as razon', 'plazas.plaza', 'zonas.zona' )
                ->where('users_empleados.users_Id', "=", $user_Id)
                ->where('empleados.activo', "=", 1)
                ->get();
        }else
        {
            $sql  = DB::table('empleados')

                ->leftjoin('razon_social', function ($join) {
                    $join->on('empleados.razon_Id', '=', 'razon_social.Id');
                })
                ->leftjoin('plazas', function ($join) {
                    $join->on('empleados.plaza_Id', '=', 'plazas.Id');
                })
                ->leftjoin('zonas', function ($join) {
                    $join->on('empleados.zona_Id', '=', 'zonas.Id');
                })
                ->leftjoin('puestos', function ($join) {
                    $join->on('empleados.puesto_Id', '=', 'puestos.Id');
                })
                ->select('empleados.*','puestos.puesto', 'razon_social.nombre as razon', 'plazas.plaza', 'zonas.zona' )
                ->where('empleados.activo', "=", 1)
                ->get();

        }


        return $sql;
    }

    public static function setPathDownloads()
    {
        $path[] = public_path(); //para localhost
        $path[] = '/home/ventum/public_test/'; //para pruebas
        $path[] = '/home/ventum/public_html/'; //para productivo
        return $path[2];
    }

    public static function returnUrl()
    {
        $url[] = "localhost:8000";//para localhost
        $url[] = "test.ventumsupervision.com";//para pruebas
        $url[] = "ventumsupervision.com";//para productivo
        return $url[2];
    }
    //listamos tiendas con filtros de razon, plaza y tienda
    public static function listaTiendasFiltro($user_Id, $razon, $plaza, $tienda) {

        $sql  = DB::table('users_tiendas')
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
            })
            ->leftjoin('razon_social', function ($join) {
                $join->on('tiendas.razon_Id', '=', 'razon_social.Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('tiendas.plaza_Id', '=', 'plazas.Id');
            })
            ->select('tiendas.*', 'users_tiendas.Id as usti_id', 'razon_social.nombre as razon', 'plazas.plaza' );

        if($razon != 0)
        {
            $sql->where('tiendas.razon_Id', "=", $razon);
        }
        if($plaza != 0)
        {
            $sql->where('tiendas.plaza_Id', "=", $plaza);
        }

        if($tienda != 0)
        {
            $sql->where('tiendas.Id', "=", $tienda);
        }
        $sql->where('users_tiendas.users_Id', "=", $user_Id)
            ->where('tiendas.activo', "=", 1);

        $data = $sql->get();

        return $data;
    }

    /**funcion que mostrara las familias asignadas**/

    public static function listaFamAsignadas($user, $empresas_Id)
    {
        $arrRes = array();
        $sql = DB::table('familias');
        $sql ->where('familias.empresas_Id','=',$empresas_Id);
        $data = $sql->get();
        $familias = collect($data)->toArray();
        for($i = 0;$i<count($familias );$i++)
        {
            $familias[$i]->Id;
            $sql  = DB::table('users_familias')
                ->leftjoin('familias', function ($join) {
                    $join->on('familias.Id', '=', 'users_familias.familias_Id');
                })->select('familias.*');
            $sql ->where('users_familias.users_Id','=',$user);
            $sql ->where('users_familias.familias_Id','=',$familias[$i]->Id);
            $count = $sql->count();
            $fam = $sql->first();
            if($count != 0)
            {
                $arrRes[] = array("Id" => $familias[$i]->Id, "Nombre" => $familias[$i]->Nombre, "Asignado" => 1);
            }else
            {
                $arrRes[] = array("Id" => $familias[$i]->Id, "Nombre" => $familias[$i]->Nombre, "Asignado" => 0);
            }
        }
        return $arrRes;
    }

    /**funcion que mostrara los empleados aun no asignados**/

    public static function listaEmpNoAsignados($user, $empresas_Id, $razon, $plaza, $sucursal, $puesto)
    {
        $arrRes = array();
        $sql = DB::table('empleados')
            ->leftjoin('razon_social', function ($join) {
                $join->on('empleados.razon_Id', '=', 'razon_social.Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('empleados.plaza_Id', '=', 'plazas.Id');
            })
            ->leftjoin('zonas', function ($join) {
                $join->on('empleados.zona_Id', '=', 'zonas.Id');
            })
            ->leftjoin('puestos', function ($join) {
                $join->on('empleados.puesto_Id', '=', 'puestos.Id');
            })
            ->select('empleados.*', 'puestos.puesto', 'razon_social.nombre as razon', 'plazas.plaza', 'zonas.zona' );
        $sql ->where('empleados.empresas_Id','=',$empresas_Id);
        $sql ->where('empleados.activo','=',1);

        //filtros
        if($razon != 0)
        {
            if($plaza != 0)
            {
                $sql ->where('empleados.plaza_Id','=',$plaza);


            }else
            {
                $sql ->where('empleados.razon_Id','=',$razon);
            }

        }

        if($puesto != 0)
        {
            $sql ->where('empleados.puesto_Id','=',$puesto);
        }


        $data = $sql->get();
        $empleados = collect($data)->toArray();
        for($i = 0;$i<count($empleados);$i++)
        {
            $empleados[$i]->Id;
            $sql  = DB::table('users_empleados')
                ->leftjoin('empleados', function ($join) {
                    $join->on('empleados.Id', '=', 'users_empleados.empleados_Id');
                })->select('empleados.*');
            $sql ->where('users_empleados.users_Id','=',$user);
            $sql ->where('users_empleados.empleados_Id','=',$empleados[$i]->Id);
            $count = $sql->count();
            $fam = $sql->first();
            if($count == 0)
            {
                $arrRes[] = array("Id" => $empleados[$i]->Id,
                                    "Nombre" => $empleados[$i]->nombre." ". $empleados[$i]->apepat." ".  $empleados[$i]->apemat ,
                                    "Puesto" => $empleados[$i]->puesto,
                                    "Razon" => $empleados[$i]->razon,
                                    "Plaza" => $empleados[$i]->plaza,
                                    "Zona" => $empleados[$i]->zona,
                                    "Clave" => $empleados[$i]->clave
                );
            }
        }
        return $arrRes;
    }



    /**funcion que mostrara los sucurcales aun no asignados**/

    public static function listaSucNoAsignados($user, $empresas_Id, $razon, $plaza, $sucursal)
    {
        $arrRes = array();
        $sql = DB::table('tiendas')
            ->leftjoin('razon_social', function ($join) {
                $join->on('tiendas.razon_Id', '=', 'razon_social.Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('tiendas.plaza_Id', '=', 'plazas.Id');
            })
            ->leftjoin('zonas', function ($join) {
                $join->on('tiendas.zona', '=', 'zonas.Id');
            })
            ->select('tiendas.*', 'razon_social.nombre as razon', 'plazas.plaza', 'zonas.zona' );
        $sql ->where('tiendas.empresas_Id','=',$empresas_Id);
        $sql ->where('tiendas.activo','=',1);

        //filtros
        if($razon != 0)
        {
            if($plaza != 0)
            {
                $sql ->where('tiendas.plaza_Id','=',$plaza);
            }else
            {
                $sql ->where('tiendas.razon_Id','=',$razon);
            }

        }

        $data = $sql->get();
        $tiendas = collect($data)->toArray();
        for($i = 0;$i<count($tiendas);$i++)
        {
            $tiendas[$i]->Id;
            $sql  = DB::table('users_tiendas')
                ->leftjoin('tiendas', function ($join) {
                    $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
                })->select('tiendas.*');
            $sql ->where('users_tiendas.users_Id','=',$user);
            $sql ->where('users_tiendas.tiendas_Id','=',$tiendas[$i]->Id);
            $count = $sql->count();
            $fam = $sql->first();
            if($count == 0)
            {
                $arrRes[] = array("Id" => $tiendas[$i]->Id,
                    "Nombre" => $tiendas[$i]->nombre ,
                    "Clave" => $tiendas[$i]->numsuc ,
                    "Razon" => $tiendas[$i]->razon ,
                    "Plaza" => $tiendas[$i]->plaza ,
                    "Zona" => $tiendas[$i]->zona ,
                );
            }
        }
        return $arrRes;
    }


    /**Funcion que cargara a todos los usuarios diponibles**/
    public static function listaUsuarios($empresas_Id) {

        //cargamos los usuarios disponibles
        $sql = DB::table('users')
            ->where('users.empleados_Id','<>',0)
            ->where('users.tipo_user','=',1)
            ->where('users.Activo','=',1)
            ->where('users.empresas_Id','=',$empresas_Id)
            ->orderBy('users.name', 'asc')
            ->get();
        return $sql;
    }

    /**Funcion que cargara a todos los puestos diponibles**/
    public static function listaPuestos($empresas_Id) {

        //cargamos los puestos disponibles
        $sql = DB::table('puestos')
            ->where('puestos..empresas_Id','=',$empresas_Id)
            ->orderBy('puestos.puesto', 'asc')
            ->get();
        return $sql;
    }

    /**Funcion que cargara a todos las familias diponibles**/
    public static function listaFamilias($empresas_Id)
    {
        //cargamos los usuarios disponibles
        $sql = DB::table('familias')
            ->where('familias.empresas_Id','=',$empresas_Id)
            ->orderBy('familias.Nombre', 'asc')
            ->get();


        return $sql;
    }


    /**Funcion que cargara a todos las familias diponibles**/
    public static function listaPlazas($empresas_Id)
    {
        //cargamos los usuarios disponibles
        $sql = DB::table('plazas')
            ->leftjoin('razon_social', function ($join) {
                $join->on('razon_social.Id', '=', 'plazas.razon_social_Id');
            })->select('plazas.*', 'razon_social.nombre as razon')
            ->where('razon_social.empresas_Id','=',$empresas_Id)
            ->orderBy('plazas.plaza', 'asc')
            ->get();


        return $sql;
    }



    /**Funcion que siempre nos entregará la
     * lista de empleados tipo supervisor encontrados y asignados a un usuario X*
     */
    public static function listaSupervisores($users_Id) {

        //cargamos los supervisores

        $sql = DB::table('emp_puestos')
            ->leftjoin('empleados', function ($join) {
                $join->on('empleados.puesto_Id', '=', 'emp_puestos.puestos_Id');
            })
            ->leftjoin('users_empleados', function ($join) {
                $join->on('empleados.Id', '=', 'users_empleados.empleados_Id');
            })
            ->leftjoin('puestos', function ($join) {
                $join->on('puestos.Id', '=', 'empleados.puesto_Id');
            })
            ->leftjoin('razon_social', function ($join) {
                $join->on('razon_social.Id', '=', 'empleados.razon_Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('plazas.Id', '=', 'empleados.plaza_Id');
            })
            ->leftjoin('users', function ($join) {
                $join->on('users.empleados_Id', '=', 'empleados.Id');
            })
            ->select('empleados.*', 'puestos.puesto', 'users.id as supervisor_Id' , 'users.name')
            ->where('emp_puestos.tipo_puesto_Id','=',1)

            ->where('users_empleados.users_Id','=',$users_Id)
            ->orderBy('razon_social.nombre', 'asc')
            ->orderBy('plazas.plaza', 'asc')
            ->orderBy('puestos.puesto', 'asc')
            ->orderBy('empleados.nombre', 'asc')
            ->get();

        return $sql;
    }

    /**Funcion que buscara los supervisores asignados a una tienda*
     */
    public static function listaSupervisoresSuc($tiendas_Id) {

        //obtenemos los datos de la tienda

        $suc =  DB::table('tiendas')->where('tiendas.Id','=',$tiendas_Id)->first();
        //cargamos los supervisores


        $sql = DB::table('empleados')
            ->leftjoin('emp_puestos', function ($join) {
                $join->on('empleados.puesto_Id', '=', 'emp_puestos.puestos_Id');
            })->leftjoin('puestos', function ($join) {
                $join->on('puestos.Id', '=', 'empleados.puesto_Id');
            })
            ->leftjoin('razon_social', function ($join) {
                $join->on('razon_social.Id', '=', 'empleados.razon_Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('plazas.Id', '=', 'empleados.plaza_Id');
            })
            ->leftjoin('users', function ($join) {
                $join->on('users.empleados_Id', '=', 'empleados.Id');
            })
            ->select('empleados.*', 'puestos.puesto', 'users.id as supervisor_Id' )
            ->where('emp_puestos.tipo_puesto_Id','=',1)
            ->where('empleados.plaza_Id','=',$suc->plaza_Id)

            ->orderBy('razon_social.nombre', 'asc')
            ->orderBy('plazas.plaza', 'asc')
            ->orderBy('puestos.puesto', 'asc')
            ->orderBy('empleados.nombre', 'asc')
            ->get();

        return $sql;
    }

    /**
     * Funcion que regresa un listado de to-do enlistado por antiguedad
     **/

    public static function listaTodoAntiguedad($tiendas_Id) {

        $sql  =  DB::table('visitas_todo')
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'visitas.checklist_Id');
            })
            ->leftjoin('campos', function ($join) {
                $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
            })
            ->leftjoin('categorias', function ($join) {
                $join->on('categorias.Id', '=', 'campos.categorias_Id');
            })
            ->select('visitas_todo.*', 'tiendas.nombre as tienda', 'tiendas.numsuc', 'checklist.nombre as checklist',  'campos.nombre as campo', 'categorias.nombre as categoria' )
            ->where('visitas.Status','=',131)
            ->where('visitas_todo.Status','<>',154)
            ->where('tiendas.Id', "=", $tiendas_Id)
            ->orderByRaw('visitas_todo.fecha_inicio ASC')
            ->get();

        return $sql;
    }


    /**
     * Funcion que regresa los checklist que tiene asignado un usuario
     **/

    public static function listaChecklist($empleados_Id, $empresas_Id) {
        //buscamos el id del puesto
        $sql = DB::table('empleados');
        $sql->where('empleados.Id','=',$empleados_Id);
        $puesto = $sql->first();


        if($empleados_Id == 0)
        {
            $sql  =  DB::table('checklist')
                ->where('checklist.empresas_Id','=',$empresas_Id)
                ->where('checklist.activo','=',1)
                ->get();
        }else
        {

            $data  = DB::table('chk_puesto')
                ->leftjoin('checklist', function ($join) {
                    $join->on('checklist.Id', '=', 'chk_puesto.checklist_Id');
                })
                ->select('checklist.*' );
            $data->where('chk_puesto.puestos_Id', "=", $puesto->puesto_Id);
            $data->where('checklist.activo','=',1);
            $sql  = $data->get();

        }

        return $sql;
    }

    /**
     * Funcion que regresa los cuestionarios que tiene asignado un usuario
     **/

    public static function listaCuestionarios( $empresas_Id) {

            $sql  =  DB::table('cuestionario')
                ->where('cuestionario.empresas_Id','=',$empresas_Id)
                ->get();

        return $sql;
    }

    /**
     * Funcion que regresa los razones de una empresa que tiene asignado un usuario
     **/

    public static function razones( $empresas_Id) {
        //buscamos el id del puesto
        $sql = DB::table('razon_social');
        $sql->where('razon_social.empresas_Id','=',$empresas_Id);
        $data = $sql->get();
        $razones = collect($data)->toArray();

        return $razones;
    }

    /**
     * Funcion que regresa los zonas de una empresa que tiene asignado un usuario
     **/

    public static function zonas( $empresas_Id) {
        //buscamos el id del puesto
        $sql = DB::table('zonas');
        $sql->where('zonas.empresas_Id','=',$empresas_Id);
        $data = $sql->get();
        $zonas= collect($data)->toArray();

        return $zonas;
    }

    public static function getMes($mes) {

        $mes = intval($mes);
        $arrMes = array("",
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre");


        return $arrMes[$mes];
    }

    public static function getTipoTodo($tipo) {
        $tipo_string = "";
        switch($tipo)
        {
            case 1:
                $tipo_string = "Visita";
                break;
            case 2:
                $tipo_string = "Express";
                break;
            case 3:
                $tipo_string = "Ftp";
                break;
            case 4:
                $tipo_string = "Supervisor";
                break;
        }

        return $tipo_string;
    }

    public static function getStatus($status) {

        $stat = array();
        $status_string = "";
        $status_label = "";
        switch($status)
        {
            case 151:
                $status_string = "Nuevo";
                $status_label = "danger";
                break;
            case 152:
                $status_string = "En Curso";
                $status_label = "warning";
                break;
            case 153:
                $status_string = "Pendiente";
                $status_label = "primary";
                break;
            case 154:
                $status_string = "Concluida";
                $status_label = "success";
                break;
        }

        return $stat = array($status_string,$status_label);
    }

    public static function filtraStatus($nvo, $cur, $pen, $aut, $close)
    {
        $suma = $nvo + $cur + $pen + $aut + $close;
        $arrRes = array();
        //switch de seleccion
        switch($suma)
        {
            case 0:$arrRes = array(151,152,153,154,155);break;
            case 3:$arrRes = array(151,152,153,154,155);break;
            case 765:$arrRes = array(151,152,153,154,155);break;
            case 610:$arrRes = array(151,152,153,154);break;
            case 456:$arrRes = array(151,152,153);break;
            case 303:$arrRes = array(151,152);break;
            case 151:$arrRes = array(151);break;
            case 614:$arrRes = array(152,153,154,155);break;
            case 459:$arrRes = array(152,153,154);break;
            case 305:
                if($close == 154)
                {
                    $arrRes = array(151,154);
                }else
                {
                    $arrRes = array(152,153);
                }

                break;
            case 152:$arrRes = array(152);break;
            case 613:$arrRes = array(151,153,154,155);break;
            case 458:$arrRes = array(151,153,154);break;
            case 304:$arrRes = array(151,153);break;
            case 153:$arrRes = array(153);break;
            case 612:$arrRes = array(151,152,154,155);break;
            case 457:$arrRes = array(151,152,154);break;
            case 154:$arrRes = array(154);break;
            case 611:$arrRes = array(151,152,153,155);break;
            case 460:$arrRes = array(152,153,155);break;
            case 308:$arrRes = array(153,155);break;
            case 155:$arrRes = array(155);break;
            case 309:$arrRes = array(155, 154);break;
            case 306:$arrRes = array(151, 155);break;
            case 307:$arrRes = array(152, 155);break;


        }

        return $arrRes;
    }

    public static function filtraStatusNombre($nvo, $cur, $pen, $aut, $close)
    {
        $suma = $nvo + $cur + $pen + $aut + $close;
        $arrRes = "";
        //switch de seleccion
        switch($suma)
        {
            case 0:
            case 3:
            case 765:$arrRes = "Todos";break;
            case 610:$arrRes = "Nuevos, En curso, Pendiente, Concluido";break;
            case 456:$arrRes = "Nuevos, En curso, Pendiente";break;
            case 303:$arrRes = "Nuevos, En curso";break;
            case 151:$arrRes = "Nuevos";break;
            case 614:$arrRes = "Nuevos, Pendientes, Concluidos, Por Autorizar";break;
            case 459:$arrRes = "Nuevos, Pendientes, Concluidos";break;
            case 305:
                if($close == 154)
                {
                    $arrRes = "Nuevos, Concluidos";
                }else
                {
                    $arrRes = "En curso, Pendientes";
                }

                break;
            case 152:$arrRes = "En curso";break;
            case 613:$arrRes = "Nuevos, Pendientes, Concluidos, Por autorizar";break;
            case 458:$arrRes = "Nuevos, Pendientes, Concluidos";break;
            case 304:$arrRes = "Nuevos, Pendientes";break;
            case 153:$arrRes = "Pendientes";break;
            case 612:$arrRes = "Nuevos, En curso, Concluidos, Por autorizar";break;
            case 457:$arrRes = "Nuevos, En curso, Concluidos";break;
            case 154:$arrRes = "Concluidos";break;
            case 611:$arrRes = "Nuevos, En curso, Pendientes, Por autorizar";break;
            case 460:$arrRes = "En curso, Pendientes, Por autorizar";break;
            case 308:$arrRes = "Pendientes, Por Autorizar";break;
            case 155:$arrRes = "Por autorizar";break;
            case 309:$arrRes = "Por autorizar, Concluidos";break;
            case 306:$arrRes = "Nuevos, Por autorizar";break;
            case 307:$arrRes = "En curso, Por autorizar";break;


        }

        return $arrRes;
    }

    /**funcion que nos regresará nombres especificos de filtros de reportes**/
    public static function getNombres($tipo, $id, $opt = "")
    {
        $nombre = "Todas";
        switch($tipo)
        {
            case 0:

                if($id != 0)
                {
                    $data  = DB::table('razon_social')
                        ->select('razon_social.nombre' );
                    $data->where('razon_social.Id', "=", $id);
                    $sql  = $data->first();
                    $nombre = $sql->nombre;
                }

                break;
            case 1:
                if($id != 0)
                {
                    $data  = DB::table('plazas')
                        ->select('plazas.plaza' );
                    $data->where('plazas.Id', "=", $id);
                    $sql  = $data->first();
                    $nombre = $sql->plaza;
                }
                break;
            case 2:
                if($id != 0)
                {
                    $data  = DB::table('tiendas')
                        ->select('tiendas.nombre' );
                    $data->where('tiendas.Id', "=", $id);
                    $sql  = $data->first();
                    $nombre = $sql->nombre;
                }
                break;
            case 3:
                if($id != 0)
                {
                    $data  = DB::table('users')
                        ->select('users.name');
                    $data->where('users.id', "=", $id);
                    $sql  = $data->first();
                    $nombre = $sql->name;
                }
                break;
            case 4:
                if($id == 0 && $opt == 0)
                {
                    $nombre = "Checklist y Seguimiento";
                }else if($id == 1 && $opt == 0)
                {
                    $nombre = "Checklist";
                }
                else if($id == 0 && $opt == 1)
                {
                    $nombre = "Seguimiento";
                }else if($id == 1 && $opt == 1)
                {
                    $nombre = "Checklist y Seguimiento";
                }
                break;
            case 5:
                $nombre = "del ". date('d/m/Y', strtotime($id)). " al ". date('d/m/Y', strtotime($opt));
                break;
            case 6:
                if($id != 0)
                {
                    $data  = DB::table('familias')
                        ->select('familias.Nombre');
                    $data->where('familias.Id', "=", $id);
                    $sql  = $data->first();
                    $nombre = $sql->Nombre;
                }
                break;
            case 7:
                if($id != 0)
                {
                    $data  = DB::table('checklist')
                        ->select('checklist.nombre');
                    $data->where('checklist.Id', "=", $id);
                    $sql  = $data->first();
                    $nombre = $sql->nombre;
                }
                break;
        }
        return $nombre;
    }

}