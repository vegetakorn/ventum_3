<?php

namespace App\Http\Controllers\Supervision;

use App\Helpers\Formulas;
use App\Helpers\Listados;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActividadesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listado = New Listados();
        $data['supervisores'] = $listado->listaSupervisores(auth()->user()->id);

        return view('actividades.lista')->with( $data);
    }


    public function getActividadFiltro(Request $request)
    {
        $formulas = new Formulas();

        $sql  = DB::table('actividades')
            ->leftjoin('actividad_empleado', function ($join) {
                $join->on('actividad_empleado.actividad_Id', '=', 'actividades.Id');
            })
            ->leftjoin('empleados', function ($join) {
                $join->on('actividad_empleado.empleados_Id', '=', 'empleados.Id');
            })
            ->leftjoin('users_empleados', function ($join) {
                $join->on('users_empleados.empleados_Id', '=', 'empleados.Id');
            })
            ->select('actividades.*');
        $sql->where('actividad_empleado.empleados_Id', "=", $request['supervisor']);
        $sql->where('users_empleados.users_Id', "=", auth()->user()->id);

        if($request['abierto'] == 1 && $request['cerrado'] == 1)
        {
            $sql ->whereIn('actividades.status',array(151,152,153, 154));

        }else if($request['abierto'] == 1 && $request['cerrado'] == 0)
        {
            $sql ->whereIn('actividades.status',array(151,152,153));

        }else if($request['abierto'] == 0 && $request['cerrado'] == 1)
        {
            $sql ->whereIn('actividades.status',array(154));

        }else if($request['abierto'] == 0 && $request['cerrado'] == 0)
        {
            $sql ->whereIn('actividades.status',array(151,152,153, 154));
        }



        $fechas = explode("-",$request['fecha'] );

        $fechaini = explode('/', $fechas[0]);
        $fechafin = explode('/', $fechas[1]);
        $sql ->whereBetween('actividades.fhinicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])));
        $sql->groupBy('actividades.Id');
        $data = $sql->get();

        $actividades = collect($data)->toArray();

        $acts = 0;
        if(count($actividades) != 0)
        {
            for($i = 0;$i<count($actividades);$i++)
            {
                $sups = array();
                $actId = $actividades[$i]->Id;
                $datasup =   DB::table('actividad_empleado')
                    ->leftjoin('empleados', function ($join) {
                        $join->on('empleados.Id', '=', 'actividad_empleado.empleados_Id');
                    })
                    ->leftjoin('users', function ($join) {
                        $join->on('empleados.Id', '=', 'users.empleados_Id');
                    })
                    ->select('empleados.*', 'actividad_empleado.Id as acsupId', 'users.id as user_Id')
                    ->where('actividad_empleado.actividad_Id','=',$actId)->get();
                $supervisores = collect($datasup)->toArray();
                $avance_tot = 0;
                $dias = 0;
                $no_sups = 0;
                for($j = 0;$j<count($supervisores);$j++)
                {
                    $avance = 0;
                    //aqui se va a calcular el avance por usuario
                    //sumamos el total de su avance
                    $sql  = DB::table('actividad_comentario');
                    $sql->where('actividad_comentario.actividad_Id', "=", $actividades[$i]->Id);
                    $sql->where('actividad_comentario.user_Id', "=", $supervisores[$j]->user_Id);
                    $data = $sql->get();
                    $actividades_com = collect($data)->toArray();
                    //sumamos las actividades
                    for($k = 0;$k<count($actividades_com);$k++)
                    {
                        $avance = $avance + $actividades_com[$k]->avance;
                    }
                    $no_sups++;
                    $avance_tot = $avance_tot + $avance;

                    $sups[] = array("Id" => $supervisores[$j]->Id,
                        "Supervisor" => $supervisores[$j]->nombre." ".$supervisores[$j]->apepat." ".$supervisores[$j]->apemat,
                        "Avance" => $avance);

                }
                $fecha2 = date('Y-m-d');
                $dias =  $formulas->diferenciaDias($actividades[$i]->fhinicio, $fecha2);
                $prom_avance = round($avance_tot / $no_sups, 2);

                $acts[] = array("Id" => $actividades[$i]->Id,
                    "actividad" => $actividades[$i]->actividad,
                    "descripcion" => $actividades[$i]->descripcion,
                    "foto" => $actividades[$i]->foto,
                    "status" => $actividades[$i]->status,
                    "avance" => $prom_avance,
                    "Dias"   => $dias,
                    "fhinicio"   => date('d/m/Y', strtotime($actividades[$i]->fhinicio)),
                    "supervisores" => $sups);



            }
        }else
        {
            $acts = 0;
        }


        return response()->json(['message' => $acts] );
    }
}
