<?php

namespace App\Http\Controllers;

use App\Helpers\Listados;
use Illuminate\Http\Request;

class PanelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function panel()
    {
        $listado = New Listados();
        $data['usuarios'] = $listado->listaUsuarios(auth()->user()->empresas_Id);
        return view('panel.panel')->with( $data);
    }


    public function correos()
    {
        $listado = New Listados();
        $data['puestos'] = $listado->listaPuestos( auth()->user()->empresas_Id);
        $data['razones'] = $listado->razones(auth()->user()->empresas_Id);
        return view('panel.correos')->with( $data);
    }
}
