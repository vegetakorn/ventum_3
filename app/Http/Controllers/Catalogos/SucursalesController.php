<?php

namespace App\Http\Controllers\Catalogos;

use App\Helpers\Listados;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SucursalesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listado = New Listados();
        $data['sucursales'] = $listado->listaTiendas(auth()->user()->id, auth()->user()->empleados_Id);

        return view('sucursales.lista')->with( $data);
    }
}
