<?php

namespace App\Http\Controllers\Catalogos;

use App\Helpers\Listados;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChecklistController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listado = New Listados();
        $data['checklist'] = $listado->listaChecklist(auth()->user()->empleados_Id, auth()->user()->empresas_Id);

        return view('checklist.lista')->with( $data);
    }
}
