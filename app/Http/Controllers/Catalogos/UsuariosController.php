<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use App\Helpers\Listados;

class UsuariosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listado = New Listados();
        $data['usuarios'] = $listado->listaUsuarios(auth()->user()->empresas_Id);

        return view('usuarios.lista')->with( $data);
    }

    public function edit($id)
    {
        $listado = New Listados();
        $data['id'] = $id;
        $data['info'] =  DB::table('users')->where('users.id','=', $id)->first();
        $data['empleados'] = $listado->listaEmpleados($id, 1);
        $data['tiendas'] = $listado->listaTiendas($id, 1);
        $data['familias'] = $listado->listaFamilias(auth()->user()->empresas_Id);
        $data['fam_asig'] = $listado->listaFamAsignadas($id, auth()->user()->empresas_Id);
        return view('usuarios.edit')->with( $data);
    }

    public function addemp($id)
    {
        $listado = New Listados();
        $data['id'] = $id;
        $data['puestos'] = $listado->listaPuestos( auth()->user()->empresas_Id);
        $data['razones'] = $listado->razones(auth()->user()->empresas_Id);
        return view('usuarios.addemp')->with( $data);
    }

    public function addsuc($id)
    {
        $listado = New Listados();
        $data['id'] = $id;
        $data['razones'] = $listado->razones(auth()->user()->empresas_Id);
        return view('usuarios.addsuc')->with( $data);
    }

    public function asigEmp(Request $request)
    {
        if($request['flag'] == "true")
        {
            //insertamos empleado
            DB::table('users_empleados')->insert([
                [
                    'users_Id' =>  $request['id'],
                    'empleados_Id' =>  $request['id_emp'],
                ]
            ]);
            $msg = "Asignado con Éxito";
        }else
        {
            //quitamos familia
            DB::table('users_empleados')
                ->where('users_Id', $request['id'])
                ->where('empleados_Id', $request['id_emp'])
                ->delete();
            $msg = "Retirado con Éxito";
        }
        return response()->json(['message' =>  $request->all()]);
    }

    public function asigSuc(Request $request)
    {
        if($request['flag'] == "true")
        {
            //insertamos empleado
            DB::table('users_tiendas')->insert([
                [
                    'users_Id' =>  $request['id'],
                    'tiendas_Id' =>  $request['id_suc'],
                ]
            ]);
            $msg = "Asignado con Éxito";
        }else
        {
            //quitamos familia
            DB::table('users_tiendas')
                ->where('users_Id', $request['id'])
                ->where('tiendas_Id', $request['id_suc'])
                ->delete();
            $msg = "Retirado con Éxito";
        }
        return response()->json(['message' =>  $request->all()]);
    }

    public function quitAll(Request $request)
    {
        //quitamos familia
        DB::table('users_empleados')
            ->where('users_Id', $request['id'])
            ->delete();
        $msg = "Retirado con Éxito";

        return response()->json(['message' =>  $request->all()]);
    }

    public function quitAllSuc(Request $request)
    {
        //quitamos familia
        DB::table('users_tiendas')
            ->where('users_Id', $request['id'])
            ->delete();
        $msg = "Retirado con Éxito";

        return response()->json(['message' =>  $request->all()]);
    }

    public function addFiltroEmp(Request $request)
    {
        $razon  = $request['razid'];
        $plaza  = $request['plaid'];
        $puesto = $request['ptoid'];
        $user = $request['id'];
        $arrRes = array();
        $sql = DB::table('empleados')
            ->leftjoin('razon_social', function ($join) {
                $join->on('empleados.razon_Id', '=', 'razon_social.Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('empleados.plaza_Id', '=', 'plazas.Id');
            })
            ->leftjoin('zonas', function ($join) {
                $join->on('empleados.zona_Id', '=', 'zonas.Id');
            })
            ->leftjoin('puestos', function ($join) {
                $join->on('empleados.puesto_Id', '=', 'puestos.Id');
            })
            ->select('empleados.*', 'puestos.puesto', 'razon_social.nombre as razon', 'plazas.plaza', 'zonas.zona' );
        $sql ->where('empleados.empresas_Id','=',auth()->user()->empresas_Id);
        $sql ->where('empleados.activo','=',1);

        //filtros
        if($razon != 0)
        {
            if($plaza != 0)
            {
                $sql ->where('empleados.plaza_Id','=',$plaza);


            }else
            {
                $sql ->where('empleados.razon_Id','=',$razon);
            }

        }

        if($puesto != 0)
        {
            $sql ->where('empleados.puesto_Id','=',$puesto);
        }


        $data = $sql->get();
        $empleados = collect($data)->toArray();
        for($i = 0;$i<count($empleados);$i++)
        {
            $empleados[$i]->Id;
            $sql  = DB::table('users_empleados')
                ->leftjoin('empleados', function ($join) {
                    $join->on('empleados.Id', '=', 'users_empleados.empleados_Id');
                })->select('empleados.*');
            $sql ->where('users_empleados.users_Id','=',$user);
            $sql ->where('users_empleados.empleados_Id','=',$empleados[$i]->Id);
            $count = $sql->count();
            $fam = $sql->first();
            if($count == 0)
            {
                //agregamos el empleado correspondiente
                //insertamos empleado
                DB::table('users_empleados')->insert([
                    [
                        'users_Id' =>  $user,
                        'empleados_Id' =>  $empleados[$i]->Id,
                    ]
                ]);

            }
        }

        return response()->json(['message' =>  $request->all()]);
    }

    public function addFiltroSuc(Request $request)
    {
        $razon  = $request['razid'];
        $plaza  = $request['plaid'];
        $user = $request['id'];
        $arrRes = array();
        $sql = DB::table('tiendas')
            ->leftjoin('razon_social', function ($join) {
                $join->on('tiendas.razon_Id', '=', 'razon_social.Id');
            })
            ->leftjoin('plazas', function ($join) {
                $join->on('tiendas.plaza_Id', '=', 'plazas.Id');
            })
            ->leftjoin('zonas', function ($join) {
                $join->on('tiendas.zona', '=', 'zonas.Id');
            })
            ->select('tiendas.*', 'razon_social.nombre as razon', 'plazas.plaza', 'zonas.zona' );
        $sql ->where('tiendas.empresas_Id','=',auth()->user()->empresas_Id);
        $sql ->where('tiendas.activo','=',1);

        //filtros
        if($razon != 0)
        {
            if($plaza != 0)
            {
                $sql ->where('tiendas.plaza_Id','=',$plaza);


            }else
            {
                $sql ->where('tiendas.razon_Id','=',$razon);
            }

        }

        $data = $sql->get();
        $tiendas = collect($data)->toArray();
        for($i = 0;$i<count($tiendas);$i++)
        {
            $tiendas[$i]->Id;
            $sql  = DB::table('users_tiendas')
                ->leftjoin('tiendas', function ($join) {
                    $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
                })->select('tiendas.*');
            $sql ->where('users_tiendas.users_Id','=',$user);
            $sql ->where('users_tiendas.tiendas_Id','=',$tiendas[$i]->Id);
            $count = $sql->count();
            $fam = $sql->first();
            if($count == 0)
            {
                //agregamos el empleado correspondiente
                //insertamos empleado
                DB::table('users_tiendas')->insert([
                    [
                        'users_Id' =>  $user,
                        'tiendas_Id' =>  $tiendas[$i]->Id,
                    ]
                ]);

            }
        }
        return response()->json(['message' =>  $request->all()]);
    }

    public function updateUsrFam(Request $request)
    {
        $msg = "";
        if($request['flag'] == "true")
        {
            //insertamos familia
            DB::table('users_familias')->insert([
                [
                    'users_Id' =>  $request['id'],
                    'familias_Id' =>  $request['fam'],
                ]
            ]);
            $msg = "Asignado con Éxito";
        }else
        {
            //quitamos familia
            DB::table('users_familias')
                ->where('users_Id', $request['id'])
                ->where('familias_Id', $request['fam'])
                ->delete();
            $msg = "Retirado con Éxito";
        }


        return response()->json(['message' =>  $msg]);

    }
    public function saveUsuario(Request $request)
    {
        if($request['pass'] != "")
        {
            DB::table('users')
                ->where('id', $request['id'])
                ->update([
                    'password' =>  bcrypt($request['pass']),
                    'tipo_user' =>  $request['type']
                ]);
        }else
        {
            DB::table('users')
                ->where('id', $request['id'])
                ->update([
                    'password' =>  bcrypt($request['pass']),
                    'tipo_user' =>  $request['type']
                ]);
        }


        return response()->json(['message' =>  "OK"]);
    }

    public function activarUsu(Request $request)
    {
        if($request['flag'] != 0)
        {
            DB::table('users')
                ->where('id', $request['id'])
                ->update([
                    'Activo' =>  0

                ]);
        }else
        {
            DB::table('users')
                ->where('id', $request['id'])
                ->update([
                    'Activo' =>  1

                ]);
        }


        return response()->json(['message' =>  "OK"]);
    }

}