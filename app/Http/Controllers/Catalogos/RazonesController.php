<?php

namespace App\Http\Controllers\Catalogos;

use App\Helpers\Listados;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RazonesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listado = New Listados();
        $data['razones'] = $listado->razones(auth()->user()->empresas_Id);

        return view('razones.lista')->with( $data);
    }
}
