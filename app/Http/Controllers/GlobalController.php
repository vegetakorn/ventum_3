<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Helpers\Listados;

class GlobalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPlazas(Request $request)
    {
        $sql = DB::table('plazas');
        $sql->where('plazas.razon_social_Id','=',$request['idraz']);
        $data = $sql->get();

        return response()->json(['message' =>  $data]);

    }

    public function getTiendas(Request $request)
    {
        $sql = DB::table('tiendas')
            ->leftjoin('users_tiendas', function ($join) {
                $join->on('users_tiendas.tiendas_Id', '=', 'tiendas.Id');
            })
           ;
        $sql->where('tiendas.plaza_Id','=',$request['idpla']);
        $sql->where('users_tiendas.users_Id','=',auth()->user()->id);
        $data = $sql->get();

        return response()->json(['message' =>  $data]);

    }

    public function getFiltroEmp(Request $request)
    {
        $listado = new Listados();
        $data = $listado->listaEmpNoAsignados($request['id'], auth()->user()->empresas_Id, $request['razid'], $request['plaid'], $request['sucid'], $request['ptoid']);
        return response()->json(['message' =>  $data]);

    }

    public function getFiltroSuc(Request $request)
    {
        $listado = new Listados();
        $data = $listado->listaSucNoAsignados($request['id'], auth()->user()->empresas_Id, $request['razid'], $request['plaid'], $request['sucid']);
        return response()->json(['message' =>  $data]);

    }
}
