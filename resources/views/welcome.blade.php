<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Businnes, Portfolio, Corporate">
    <meta name="Author" content="WebThemez">
    <title>Ventum Supervisión 2020</title>
    <link rel="stylesheet" href="page/css/normalize.css">
    <link rel="stylesheet" href="page/css/bootstrap.min.css">
    <link rel="stylesheet" href="page/css/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="page/elegant_font/style.css" />
    <!--[if lte IE 7]><script src="page/elegant_font/lte-ie7.js"></script><![endif]-->
    <link rel="stylesheet" href="page/css/magnific-popup.css">
    <link rel="stylesheet" href="page/css/slider-pro.css">
    <link rel="stylesheet" href="page/css/owl.carousel.css">
    <link rel="stylesheet" href="page/css/owl.theme.css">
    <link rel="stylesheet" href="page/css/owl.transitions.css">
    <link rel="stylesheet" href="page/css/animate.css">
    <link rel="stylesheet" href="page/elegant_font/style.css">
    <link rel="stylesheet" href="page/css/style.css">

    <!--[if lt IE 9]>
    <script src="page/js/html5shiv.min.js"></script>
    <script src="page/js/respond.min.js"></script>
    <script type="text/javascript" src="page/js/selectivizr.js"></script>
    <![endif]-->
</head>

<body>
<!-- Preloader -->
<div class="preloader">
    <div class="status"></div>
</div>

<!-- Header End -->
<header>
    <!-- Navigation Menu start-->

    <nav id="topNav" class="navbar navbar-default main-menu"  style="background: #9fbbcb ">
        <div class="container">
            <button class="navbar-toggler hidden-md-up pull-right" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
                ☰
            </button>
            <a class="navbar-brand page-scroll" href="#slider"><img class="logo" style="width: 200px; height: 75px" id="logo" src="page/images/logo_ventum.png" alt="logo"></a>
            <div class="collapse navbar-toggleable-sm" id="collapsingNavbar">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="#slider">INICIO</a>
                    </li>
                    <li>
                        <a href="#services">SISTEMA</a>
                    </li>
                    <li>
                        <a href="#about">NOSOTROS</a>
                    </li>

                    <li>
                        <a href="#portfolio">GALERIA</a>
                    </li>

                    <!--<li>
                        <a href="#pricing">PRICING</a>
                    </li>-->

                    <li>
                        <a href="#team">EQUIPO</a>
                    </li>
                    <li>
                        <a href="#clients">CLIENTES</a>
                    </li>
                    <li>
                        <a href="#contact">CONTACTO</a>
                    </li>
                    <li>
                        <a href="{{route('home')}}">LOGIN</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


</header>
<!-- Header End -->


<section class="slider-pro slider" id="slider">
    <div class="sp-slides">

        <!-- Slides -->
        <div class="sp-slide main-slides">
            <div class="img-overlay"></div>
            <img class="sp-image" style="width: 1924px" src="page/images/slider/slider-img-6.jpg" alt="Slider 2"/>

            <h1 class="sp-layer slider-text-big"
                data-position="center" data-show-transition="left" data-hide-transition="right" data-show-delay="1500" data-hide-delay="200">
                <span class="highlight-texts">Sistema de Supervisión de Sucursales</span>
            </h1>

            <p class="sp-layer"
               data-position="center" data-vertical="15%" data-show-delay="2000" data-hide-delay="200" data-show-transition="left" data-hide-transition="right">
                Desarrollado por expertos en la industria del retail.
            </p>
        </div>
        <!-- Slides End -->

        <!-- Slides -->
        <div class="sp-slide main-slides">
            <div class="img-overlay"></div>

            <img class="sp-image" style="width: 1924px" src="page/images/slider/slider-img-4.jpg" alt="Slider 1"/>

            <h1 class="sp-layer slider-text-big"
                data-position="center" data-show-transition="left" data-hide-transition="right" data-show-delay="1000" data-hide-delay="200">
                <span class="highlight-texts">Supervisa tus Sucursales</span>
            </h1>

            <p class="sp-layer"
               data-position="center" data-vertical="15%" data-show-delay="1500" data-hide-delay="200" data-show-transition="left" data-hide-transition="right">
                Nuestro sistema profesionaliza tus procesos de visita con una metodología probada.
            </p>
        </div>
        <!-- Slides End -->



        <!-- Slides -->
        <div class="sp-slide main-slides">
            <div class="img-overlay"></div>

            <img class="sp-image" style="width: 1924px" src="page/images/slider/slider-img-7.jpg" alt="Slider 3"/>

            <h1 class="sp-layer slider-text-big"
                data-position="center" data-show-transition="left" data-hide-transition="right" data-show-delay="1500" data-hide-delay="200">
                <span class="highlight-texts">Mejora tu toma de decisiones</span>
            </h1>

            <p class="sp-layer"
               data-position="center" data-vertical="15%" data-show-delay="2000" data-hide-delay="200" data-show-transition="left" data-hide-transition="right">
               Generación de reportes especializados en la industria de retail.
            </p>
        </div>
        <!-- Slides End -->

    </div>
</section>
<!-- Main Slider End -->
<div id="content12" data-section="content-12" class="data-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 nopaddingnomargin">
                <div class="content-center">
                    <h3>La Metodología:</h3>
                    <p>Antes: Estado general de la sucursal a visitar. </p>
                    <p>Durante: Evaluación actual del estado de la sucursal.</p>
                    <p>Después: Generación del reporte de visita para atender las áreas de oportunidad de la sucursal.</p>
                </div>
            </div>

            <div class="col-md-6 nopaddingnomargin">
                <div class="content-right">
                    <h3>La evaluación a tu sucursal totalmente personalizable<br><b>¡Sin límite de preguntas!</b></h3>
                    <!--<a href="#" class="btn btn-default-blue">Read More</a>-->
                </div>
            </div>
        </div>
    </div>
</div> <!-- /#content12 -->
<section id="services" class="section-wrapper">
    <div class="container">
        <div class="row">
            <!-- Section Header -->
            <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                <h2><span class="highlight-text">El Sistema</span></h2>
                <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">Nuestra plataforma ofrece muchas herramientas necesarias para automatizar tus procesos de supervisión</p>
            </div>
            <!-- Section Header End -->

            <div class="our-services">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".2s">
                        <div class="service-box">
                            <div class="icon">
                                <i class="icon_piechart"></i> <h3> Gráficas de Apoyo</h3>
                            </div>
                            <p>El sistema puede mostrar de manera visual los procesos del sistema: Tendencias, avances e información financiera, entre otras. </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".2s">
                        <div class="service-box">
                            <div class="icon">
                                <i class="icon_desktop"></i><h3>Plataforma WEB </h3>
                            </div>

                            <p>Sistema que incluye todas las funcionalidades del sistema, con perfiles ajustados a directores, supervisores y gerentes de tienda</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".2s">
                        <div class="service-box">
                            <div class="icon">
                                <i class="icon_currency"></i><h3>Captura de Ventas</h3>
                            </div>

                            <p>El sistema puede importar las ventas de las sucursales para generar reportes con indicadores tales como: Ticket promedio, Árticulos por Ticket, Meta Diaria, entre otros.</p>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-md-4 col-sm-4 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".1s">
                        <div class="service-box">
                            <div class="icon">
                                <i class="icon_laptop"></i><h3>Aplicación Móvil</h3>
                            </div>

                            <p>Contamos con aplicación para dispositivos móviles para realizar tus supervisiones sin la necesidad de usar internet </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".1s">
                        <div class="service-box">
                            <div class="icon">
                                <i class="icon_image"></i><h3>Soporte con Imágenes</h3>
                            </div>

                            <p>Documenta todas tus visitas con captura de fotografías sin importar el dispositivo con el cual hagas la supervisión</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 text-xs-center wow fadeInDown" data-wow-delay=".1s">
                        <div class="service-box">
                            <div class="icon">
                                <i class="icon_check"></i><h3>Personalización</h3>
                            </div>

                            <p>Adecua tus procesos de supervisión conforme a tus necesidades</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="about" class="about-sec section-wrapper description">
    <div class="container">
        <div class="row">
            <!-- Section Header -->
            <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                <h2><span class="highlight-text">Nosotros</span></h2>

                <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">Ventum es un concepto que nos ayuda a profesionalizar el proceso de supervisión de sucursales a través de un método probado a través de los años por nuestros clientes.</p>
            </div>
            <!-- Section Header End -->

            <div class="col-md-6 col-sm-6 col-xs-12 custom-sec-img wow fadeInDown">
                <img src="page/images/features.png" alt="Custom Image">
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 customized-text wow fadeInDown black-ed">
                <p>Nuestro éxito se basa gracias a las experiencias laborales de nuestros colaboradores quienes aportan sus conocimientos para garantizar la sustentabilidad de nuestra empresa entregando excelentes resultados a lo más importante para nosotros, nuestros clientes. </p>
                <p>Este éxito está basado en tres principales pilares:</p>
                <div class="row">
                    <div class="col-md-11">
                        <strong></strong>
                        <p>Somos una empresa conformada por un equipo de expertos altamente calificados en la gestión de sucursales, control de calidad y mapeo de proceso que aseguran a nuestros clientes que serán asesorados por los mejores recursos utilizando las mejores prácticas existentes</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <strong></strong>
                        <p>Nuestros valores como empresa que se ven reflejados en el día a día en nuestras operaciones</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11">
                        <strong></strong>
                        <p>El seguimiento con nuestros clientes para que alcancen sus metas</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="portfolio" class="bgSection portfolio-section">
    <div class="container">
        <div class="row">

            <!-- Section Header -->
            <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                <h2><span class="highlight-text">GALERIA DEL SISTEMA</span></h2>

                <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">La plataforma es completamente responsiva, por lo que no importa desde que dispositivo desees trabajar, el sistema siempre se adecuará a tu tecnología</p>
            </div>
            <!-- Section Header End -->

        </div>
    </div>

    <!-- Works -->
    <div class="portfolio-works wow fadeIn" data-wow-duration="2s">

        <!-- Filter Button Start -->
        <div id="portfolio-filter" class="portfolio-filter-btn-group">
            <ul>
                <li>

                </li>
            </ul>
        </div>
        <!-- Filter Button End -->

        <div class="portfolio-items">

            <!-- Portfolio Items -->
            <div class="item portfolio-item web seo">

                <img src="page/images/img-portfolio/1.png" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="page/images/img-portfolio/1.png" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Portfolio Items End -->

            <!-- Portfolio Items -->
            <div class="item portfolio-item works seo">

                <img src="page/images/img-portfolio/2.png" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="page/images/img-portfolio/2.png" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Portfolio Items End -->

            <!-- Portfolio Items -->
            <div class="item portfolio-item web">

                <img src="page/images/img-portfolio/3.png" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="page/images/img-portfolio/3.png" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Portfolio Items End -->

            <!-- Portfolio Items -->
            <div class="item portfolio-item web works brands">

                <img src="page/images/img-portfolio/4.png" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="page/images/img-portfolio/4.png" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Portfolio Items -->

            <!-- Portfolio Items -->
            <div class="item portfolio-item web brands">

                <img src="page/images/img-portfolio/5.png" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="page/images/img-portfolio/5.png" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Portfolio Items End -->

            <!-- Portfolio Items -->
            <div class="item portfolio-item works seo">

                <img src="page/images/img-portfolio/6.png" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="page/images/img-portfolio/6.png" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Portfolio Items End -->

            <div class="item portfolio-item brands seo">

                <img src="page/images/img-portfolio/7.png" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="page/images/img-portfolio/7.png" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Portfolio Items End -->

            <!-- Portfolio Items -->
            <div class="item portfolio-item web seo works">

                <img src="page/images/img-portfolio/8.png" alt="">
                <div class="portfolio-details-wrapper">
                    <div class="portfolio-details">
                        <div class="portfolio-meta-btn">
                            <ul class="work-meta">
                                <li class="lighbox"><a href="page/images/img-portfolio/8.png" class="featured-work-img"><i class="fa fa-search-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Portfolio Items End -->



        </div>
    </div>
    <!-- Works End -->


</section>
<!-- Portfolio Section End -->


<section id="info" class="info-section">
    <div class="container text-xs-center">
        <!-- Section Header -->
        <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
            <h2><span class="highlight-text">Estadísticas</span></h2>

            <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">Desde 2010 Ventum ha brindado las mejores herramientas para supervisión de sucursales, siempre adaptándose a las condiciones tecnológicas mas actuales</p>
        </div>
        <!-- Section Header End -->
        <div class="row wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
            <div class="col-md-3 col-sm-6 col-xs-12 text-xs-center">

                <i class="icon_calendar wow pulse" style="visibility: visible; animation-name: pulse;"></i>
                <h4>Experiencia</h4>
                <h1 class="text-primary">10 años en el mercado</h1>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 text-xs-center">

                <i class="icon_building wow pulse" style="visibility: visible; animation-name: pulse;"></i>
                <h4>Empresas</h4>
                <h1 class="text-primary">7 clientes activos</h1>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 text-xs-center">

                <i class="icon_cart wow pulse" style="visibility: visible; animation-name: pulse;"></i>
                <h4>Sucursales</h4>
                <h1 class="text-primary">753 con ventum implementado</h1>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 text-xs-center">

                <i class="icon_contacts wow pulse" style="visibility: visible; animation-name: pulse;"></i>
                <h4>Usuarios</h4>
                <h1 class="text-primary">959 Usuarios Activos</h1>
            </div>
        </div>
    </div>
</section>




<section id="team" class="bgSection teams-section">
    <div class="parallax-overlay"></div>
    <div class="teams-wrapper wow bounceIn">
        <div class="container">
            <div class="row">

                <!-- Section Header -->
                <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                    <h2><span class="highlight-text-inverted">Nuestro Equipo</span></h2>

                    <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">Siempre estaremos atentos a sus dudas y encontraremos la mejor solución de supervisión para su organización</p>
                </div>

                <!-- teams Slider -->
                <div id="teams" class="owl-carousel teams">

                    <!-- Slides -->
                    <div class="teams-slides col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">
                        <img src="page/images/img-teams/team5.jpg" alt="">
                        <p class="client-info">Mair Cherem</p>
                        <p>Dirección General</p>

                    </div>
                    <!-- Slides End -->

                    <!-- Slides -->
                    <div class="teams-slides col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">
                        <img src="page/images/img-teams/team6.jpg" alt="">
                        <p class="client-info">Alejandro López</p>
                        <p>Dirección General</p>

                    </div>
                    <!-- Slides End -->

                    <!-- Slides -->
                    <div class="teams-slides col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">
                        <img src="page/images/img-teams/team7.jpg" alt="">
                        <p class="client-info">Moisés Peña</p>
                        <p>Líder de Proyecto</p>

                    </div>
                    <!-- Slides End -->
                    <!-- Slides -->
                    <div class="teams-slides col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">
                        <img src="page/images/img-teams/team8.jpg" alt="">
                        <p class="client-info">Héctor Arroyo</p>
                        <p>Desarrollo en Móviles</p>

                    </div>
                    <!-- Slides End -->
                    <!-- Slides -->
                    <div class="teams-slides col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">
                        <img src="page/images/img-teams/team9.jpg" alt="">
                        <p class="client-info">Enrique García</p>
                        <p>Desarrollo en Web</p>

                    </div>
                    <!-- Slides End -->
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Clients Section -->
<section id="clients" class="clients-section">
    <!-- Container Ends -->
    <div class="container">
        <div class="row">
            <!-- Section Header -->
            <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                <h2><span class="highlight-text">Nuestros Clientes</span></h2>

                <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">Durante 10 años estas empresas nos han brindado la confianza de brindarles nuestra ayuda para alcanzar sus metas</p>
            </div>
            <!-- Section Header End -->
            <div class="wow fadeInUpQuick" data-wow-delay=".9s">
                <div class="row" id="clients-carousel">
                    <div class="client-item-wrapper">
                        <img style="height: 144px" src="page/images/clients/aditivo.png" alt="">
                    </div>
                    <div class="client-item-wrapper">
                        <img style="height: 144px" src="page/images/clients/roma.png" alt="">
                    </div>
                    <div class="client-item-wrapper">
                        <img style="height: 144px" src="page/images/clients/globo.png" alt="">
                    </div>
                    <div class="client-item-wrapper">
                        <img style="height: 144px" src="page/images/clients/comex.png" alt="">
                    </div>

                </div>
            </div>
        </div>
    </div><!-- Container Ends -->
</section>
<!-- Client Section End -->

<section id="contact" class="section-wrapper contact-section" data-stellar-background-ratio="0.5">
    <div class="parallax-overlay"></div>
    <div class="container">
        <div class="row">

            <!-- Section Header -->
            <div class="col-md-12 col-sm-12 col-xs-12 section-header wow fadeInDown">
                <h2><span class="highlight-text">¡Contáctanos!</span></h2>

                <p class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1">Déjanos tu mensaje, con gusto podemos agendar una visita a tu empresa para presentarte nuestros servicios</p>
            </div>
            <!-- Section Header End -->

            <div class="contact-details">


                <!-- Contact Form -->
                <div class="contact-form wow bounceInRight">

                    <!--NOTE: Update your email Id in "contact_me.php" file in order to receive emails from your contact form-->
                    <form name="sentMessage" id="contactForm"  novalidate>
                        <div class="col-md-6">
                            <input type="text" class="form-control"
                                   placeholder="Nombre Completo" id="name" required
                                   data-validation-required-message="¿cómo te llamas?" />
                            <p class="help-block"></p>
                        </div>
                        <div class="col-md-6">
                            <input type="email" class="form-control" placeholder="Correo Electrónico"
                                   id="email" required
                                   data-validation-required-message="Necesitamos un E-Mail para contactarte" />
                        </div>

                        <div class="col-md-12">
		<textarea rows="10" cols="100" class="form-control"
                  placeholder="Mensaje" id="message" required
                  data-validation-required-message="Puedes describir que es lo que necesitas" minlength="5"
                  data-validation-minlength-message="Minimo 5 caracteres"
                  maxlength="999" style="resize:none"></textarea>
                        </div>

                        <div class="col-md-4 col-md-offset-5"><br><div id="success"> </div><button type="submit" class="btn btn-primary">Enviar Mensaje</button></div>
                    </form>
                </div>

            </div>
        </div>
</section>




<!-- Contact Section End -->
<section class="footer-container">
    <div class="container">
        <div class="row footer-containertent">

            <div class="col-md-4 contac-us">
                <h4>Contacto</h4>
                <p>Puedes también encontrarnos aquí</p>
                <ul>
                    <li><i class="fa fa-home"></i></li>
                    <li><i class="fa fa-phone"></i>001 123 12345 99</li>
                    <li><i class="fa fa-envelope-o"></i>alopezmota@ventumsupervision.com</li>
                </ul>
            </div>
        </div>
    </div>
</section>


<footer>

    <div class="container">
        <div class="row">
            <div class="footer-containertent">

                <!--<ul class="footer-social-info">
                    <li>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-pinterest"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                    </li>
                </ul>-->
                <br/><br/>
                <p>Copyright © 2020. <a href="http://ventumsupervision.com" title="Ventum">Ventum Supervisión</a>. Todos los derechos reservados</p>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->

<script src="page/js/jquery-1.11.3.min.js"></script>
<script src="page/js/bootstrap.min.js"></script>
<script src="page/js/modernizr.min.js"></script>
<script src="page/js/jquery.easing.1.3.js"></script>
<script src="page/js/jquery.scrollUp.min.js"></script>
<script src="page/js/jquery.easypiechart.js"></script>
<script src="page/js/isotope.pkgd.min.js"></script>
<script src="page/js/jquery.fitvids.js"></script>
<script src="page/js/jquery.stellar.min.js"></script>
<script src="page/js/jquery.waypoints.min.js"></script>
<script src="page/js/wow.min.js"></script>
<script src="page/js/jquery.nav.js"></script>
<script src="page/js/imagesloaded.pkgd.min.js"></script>
<script src="page/js/smooth-scroll.min.js"></script>
<script src="page/js/jquery.magnific-popup.min.js"></script>
<script src="page/js/jquery.sliderPro.min.js"></script>
<script src="page/js/owl.carousel.min.js"></script>
<script src="page/contact/jqBootstrapValidation.js"></script>
<script src="page/contact/contact_me.js"></script>
<script src="page/js/custom.js"></script>

</body>
</html>
