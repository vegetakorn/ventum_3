@section('title_prefix')
    Ventum 3.0::Checklist
@endsection
@extends('home')
@section('body')
    <!--**********************************
            Content body start
        ***********************************-->
    <div class="content-body">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="breadcrumb-range-picker">
                    <span><i class="icon-calender"></i></span>
                    <span class="ml-1">{{date('d/m/Y')}}</span>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('checklist.lista')}}">Usuarios</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h3 class="content-heading">Administración de Checklist</h3>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="table_checklist" class="display" style="min-width: 845px">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Descripción</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($checklist as $chk)
                                            <tr>
                                                <td>{{$chk->nombre}}</td>
                                                <td>{{$chk->descripcion}}</td>
                                                @if($chk->activo == 1)
                                                    <td>Activo</td>
                                                @else
                                                    <td>Inactivo</td>
                                                @endif
                                                <td>
                                                    <span>
                                                        <a  class="mr-4" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">
                                                            <i class="fa fa-pencil color-muted"></i>
                                                        </a>
                                                        @if($chk->activo == 1)
                                                            <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Desactivar">
                                                            <i class="fa fa-close color-danger"></i>
                                                        </a>
                                                        @else
                                                            <a  data-toggle="tooltip" data-placement="top" title="" data-original-title="Activar">
                                                            <i class="fa fa-openid color-danger"></i>
                                                        </a>
                                                        @endif

                                                    </span>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Usuario</th>
                                        <th>Correo</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--**********************************
        Content body end
    ***********************************-->
@endsection
@section('css')
    <!-- JS Grid -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jsgrid/css/jsgrid.min.css')}}">
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jsgrid/css/jsgrid-theme.min.css')}}">
    <!-- Footable -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/footable/css/footable.bootstrap.min.css')}}">
    <!-- Bootgrid -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.css')}}">
    <!-- Datatable -->
    <link href="{{ asset('drora/assets/plugins/datatables/css/jquery.dataTables.min.css')}}" rel="stylesheet">
@endsection
@section('js')

    <script>

        var csrf = '{{ csrf_token() }}';
        $( document ).ready(function() {
            $('#table_checklist').DataTable( {
                "responsive": true,
                "processing": true,
                destroy: true,
                "language": {
                    "lengthMenu": "Mostrando _MENU_ registros por página ",
                    "zeroRecords": "Sin registros encontrados",
                    "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "Sin regustros encontrados",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                }
            } );
        });
    </script>
    <!--Script de request-->
    <script src="{{ asset('scripts/usuarios.js')}}"></script>
    <!-- JS Grid -->
    <script src="{{ asset('drora/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/jsgrid/js/jsgrid.min.js')}}"></script>
    <!-- Footable -->
    <script src="{{ asset('drora/assets/plugins/footable/js/footable.min.js')}}"></script>
    <!-- Bootgrid -->
    <script src="{{ asset('drora/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.js')}}"></script>
    <!-- Datatable -->
    <script src="{{ asset('drora/assets/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
    <!-- JS Grid Init -->
    <script src="{{ asset('drora/main/js/plugins-init/jsgrid-init.js')}}"></script>
    <script src="{{ asset('drora/main/js/plugins-init/footable-init.js')}}"></script>
    <script src="{{ asset('drora/main/js/plugins-init/jquery.bootgrid-init.js')}}"></script>
    <script src="{{ asset('drora/main/js/plugins-init/datatables.init.js')}}"></script>
@endsection