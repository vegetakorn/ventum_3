<!--**********************************
        Sidebar start
    ***********************************-->
<div class="nk-sidebar">
    <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">
            <li class="nav-label">Configuración</li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-speedometer"></i><span class="nav-text">Panel</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="{{route('panel.panel')}}">Generales</a></li>
                    <li><a href="{{route('panel.correos')}}">Envíos a Correo</a></li>
                    <li><a href="{{route('usuarios.lista')}}">Usuarios</a></li>

                </ul>
            </li>

            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="icon-layers"></i><span class="nav-text">Catálogos</span></a>
                <ul aria-expanded="false">
                    <li><a href="{{route('razones.lista')}}">Razones</a></li>
                    <li><a href="{{route('plazas.lista')}}">Plazas</a></li>
                    <li><a href="{{route('zonas.lista')}}">Zonas</a></li>
                    <li><a href="{{route('sucursales.lista')}}">Sucursales</a></li>
                    <li><a href="{{route('empleados.lista')}}">Empleados</a></li>
                    <li><a href="{{route('puestos.lista')}}">Puestos</a></li>
                    <li><a href="{{route('checklist.lista')}}">Checklist</a></li>
                    <li><a href="{{route('cuestionarios.lista')}}">Cuestionarios</a></li>

                </ul>
            </li>
            <li><a href="{{route('actividades.lista')}}" aria-expanded="false"><i class="icon-list"></i> <span class="nav-text">Actividades de Usuario</span></a></li>
            <li class="nav-label">Administración</li>
            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="icon-screen-smartphone"></i><span class="nav-text">Visitas</span></a>
                <ul aria-expanded="false">
                    <li><a href="{{route('test')}}">Listado de Visitas</a></li>
                    <li><a href="{{route('test')}}">Listado de To-Do's</a></li>
                    <li><a href="{{route('test')}}">Evaluaciones</a></li>
                </ul>
            </li>
            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="fa fa-shopping-cart"></i><span class="nav-text">Ventas</span></a>
                <ul aria-expanded="false">
                    <li><a href="{{route('test')}}">Ventas</a></li>
                    <li><a href="{{route('test')}}">Reporte Mensual</a></li>
                    <li><a href="{{route('test')}}">Reporte por Rango</a></li>
                </ul>
            </li>




        </ul>
    </div>
</div>
<!--**********************************
    Sidebar end
***********************************-->