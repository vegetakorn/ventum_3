<!--**********************************
        Nav header start
    ***********************************-->
<div class="nav-header">
    <div class="brand-logo">
        <a href="{{route('home')}}">
            <b class="logo-abbr">V</b>
            <span class="brand-title" style="padding-bottom: 20px"> <img style="height: 50px; width: 150px" src="{{asset('drora/assets/images/logo_ventum.png')}}" alt="Menu"></span>
        </a>
    </div>
    <div class="nav-control">
        <div class="hamburger">
            <span class="toggle-icon"><i class="icon-menu"></i></span>
        </div>
    </div>
</div>
<!--**********************************
    Nav header end
***********************************-->