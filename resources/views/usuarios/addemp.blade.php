@section('title_prefix')
    Ventum 3.0::Editar Usuario || Asignar Empleados
@endsection
@extends('home')
@section('body')
    <!--**********************************
            Content body start
        ***********************************-->
    <div class="content-body">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="breadcrumb-range-picker">
                    <span><i class="icon-calender"></i></span>
                    <span class="ml-1">{{date('d/m/Y')}}</span>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('usuarios.lista')}}">Usuarios</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('usuarios.edit', $id)}}">Edición</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('usuarios.addemp', $id)}}">Agregar Empleados</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h3 class="content-heading">Asignaciones</h3>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4 class="card-title">Buscar  Empleados</h4>
                        </div>
                        <div class="card-body">


                            <div class="basic-form">

                                    <input class="form-control" type="hidden" id="id" value="{{$id}}" >
                                    @if(count($razones) == 1)
                                        <input class="form-control" type="hidden" id="razon" value="{{$razones[0]->Id}}" >
                                    @else
                                        <div class="input-group mb-3">
                                            <p class="mb-1">Razones</p>
                                            <select id="razon" onchange="getPlazas()"  class="custom-select" >
                                                <option value="0" selected="">Todas...</option>
                                                @foreach($razones as $razon)
                                                    <option value="{{$razon->Id}}">{{$razon->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                    <div class="plazas">
                                        <input class="form-control" type="hidden" id="plaza" value="0" >
                                    </div>
                                    <div >
                                        <input class="form-control" type="hidden" id="sucursal" value="0" >
                                    </div>
                                    <div class="input-group mb-3">
                                        <p class="mb-1">Puestos</p>
                                        <select id="puesto"   class="custom-select" >
                                            <option value="0" selected="">Todos...</option>
                                            @foreach($puestos as $puesto)
                                                <option value="{{$puesto->Id}}">{{$puesto->puesto}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="mr-2 mb-2 mb-lg-0 pull-right">
                                        <button type="button" onclick="filtroEmpleados()" class="btn btn-primary primary-alert-center-top">Buscar</button>
                                    </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4 class="card-title">Asignar  Empleados</h4>
                            <div class="mr-2 mb-2 mb-lg-0 pull-right">
                                <a href="{{route('usuarios.edit', $id)}}" class="btn btn-danger danger-alert-right-bottom">Regresar</a>
                            </div>
                            <div class="mr-2 mb-2 mb-lg-0 pull-right">
                                <button onclick="agregarFiltro()" class="btn btn-success danger-alert-right-bottom">Asignar todos</button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="empleados" class="display" style="min-width: 845px">
                                    <thead>
                                    <tr>
                                        <th>Asignar</th>
                                        <th>Clave</th>
                                        <th>Nombre</th>
                                        <th>Puesto</th>
                                        <th>Razón Social</th>
                                        <th>Plaza</th>
                                        <th>Zona</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>Clave</th>
                                        <th>Nombre</th>
                                        <th>Puesto</th>
                                        <th>Razón Social</th>
                                        <th>Plaza</th>
                                        <th>Zona</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="mr-2 mb-2 mb-lg-0 pull-right">
                        <a href="{{route('usuarios.edit', $id)}}" class="btn btn-danger danger-alert-right-bottom">Regresar</a>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <!--**********************************
        Content body end
    ***********************************-->
@endsection
@section('css')
    <!-- JS Grid -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jsgrid/css/jsgrid.min.css')}}">
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jsgrid/css/jsgrid-theme.min.css')}}">
    <!-- Footable -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/footable/css/footable.bootstrap.min.css')}}">
    <!-- Bootgrid -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.css')}}">
    <!-- Datatable -->
    <link href="{{ asset('drora/assets/plugins/datatables/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <!-- Select 2 -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/select2/css/select2.min.css')}}">

@endsection
@section('js')
    <script>
        var csrf = '{{ csrf_token() }}';
        var getPla = '{{route('getPlazas')}}'
        var getSuc = '{{route('getTiendas')}}'
        var asigEmp = '{{route('asigEmp')}}'
        var getFiltroEmp = '{{route('getFiltroEmp')}}'
        var addFiltroEmp = '{{route('addFiltroEmp')}}'
        var url_edit = '{{route('usuarios.edit', $id)}}'
        var countraz = '{{count($razones)}}';

        var razid = 0;

        $( document ).ready(function()
        {
            if(countraz == 1)
            {
                razid = '{{$razones[0]->Id}}';
                getPlazas();
            }

            $('.custom-select').select2({});

            $('#empleados').DataTable( {
                "responsive": true,
                "processing": true,
                destroy: true,
                "language": {
                    "lengthMenu": "Mostrando _MENU_ registros por página ",
                    "zeroRecords": "Sin registros encontrados",
                    "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "Sin regustros encontrados",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                }
            } );


        });
    </script>
    <!--Script de request-->
    <script src="{{ asset('scripts/usuarios.js')}}"></script>
    <script src="{{ asset('scripts/globales.js')}}"></script>
    <!-- JS Grid -->
    <script src="{{ asset('drora/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/jsgrid/js/jsgrid.min.js')}}"></script>
    <!-- Footable -->
    <script src="{{ asset('drora/assets/plugins/footable/js/footable.min.js')}}"></script>
    <!-- Bootgrid -->
    <script src="{{ asset('drora/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.js')}}"></script>
    <!-- Datatable -->
    <script src="{{ asset('drora/assets/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
    <!-- JS Grid Init -->
    <script src="{{ asset('drora/main/js/plugins-init/jsgrid-init.js')}}"></script>
    <script src="{{ asset('drora/main/js/plugins-init/footable-init.js')}}"></script>
    <script src="{{ asset('drora/main/js/plugins-init/jquery.bootgrid-init.js')}}"></script>
    <script src="{{ asset('drora/main/js/plugins-init/datatables.init.js')}}"></script>
    /**para formularios**/
    <!-- Jquery Validation -->
    <script src="{{ asset('drora/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <!-- Select 2 -->
    <script src="{{ asset('drora/assets/plugins/select2/js/select2.full.min.js')}}"></script>
    <!-- Select 2 init -->
    <script src="{{ asset('drora/main/js/plugins-init/select2-init.js')}}"></script>
@endsection