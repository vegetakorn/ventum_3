@section('title_prefix')
    Ventum 3.0::Editar Usuario
@endsection
@extends('home')
@section('body')
    <!--**********************************
            Content body start
        ***********************************-->
    <div class="content-body">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="breadcrumb-range-picker">
                    <span><i class="icon-calender"></i></span>
                    <span class="ml-1">{{date('d/m/Y')}}</span>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('usuarios.lista')}}">Usuarios</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('usuarios.edit', $id)}}">Edición</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <h3 class="content-heading mt-3">Configuración de Usuario</h3>
                </div>
                <div class="col-xl-6 col-xxl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Datos Generales</h4>
                            <div class="basic-form">
                                <form>
                                    <input class="form-control" type="hidden" id="id" value="{{$info->id}}" >
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Usuario</label>
                                        <div class="col-sm-10">
                                            <input class="form-control" type="text" value="{{$info->name}}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">E-Mail</label>
                                        <div class="col-sm-10">
                                            <input class="form-control" type="text" value="{{$info->email}}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Contraseña</label>
                                        <div class="col-sm-10">
                                            <input type="password"  id="inputPass" class="form-control" placeholder="Sólo escribir si deseas cambiar contraseña">
                                        </div>
                                    </div>
                                    <fieldset class="form-group">
                                        <div class="row">
                                            <label class="col-form-label col-sm-2 pt-0">Tipo</label>
                                            <div class="col-sm-10">
                                                @if($info->tipo_user == 2)
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="gridRadios" value="2">
                                                        <label class="form-check-label">
                                                            Sucursal
                                                        </label>
                                                    </div>
                                                @else
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="gridRadios" value="1" >
                                                        <label class="form-check-label">
                                                            Administrador
                                                        </label>
                                                    </div>

                                                    <div class="form-check disabled">
                                                        <input class="form-check-input" type="radio" name="gridRadios" value="3" >
                                                        <label class="form-check-label">
                                                            Director
                                                        </label>
                                                    </div>
                                                    <div class="form-check disabled"></div>
                                                    <div class="form-check disabled">
                                                        <input class="form-check-input" type="radio" name="gridRadios" value="5" >
                                                        <label class="form-check-label">
                                                            Recursos Humanos
                                                        </label>
                                                    </div>
                                                    <div class="form-check disabled">
                                                        <input class="form-check-input" type="radio" name="gridRadios" value="6" >
                                                        <label class="form-check-label">
                                                            Departamentos
                                                        </label>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="form-group row">
                                        <div class="col-sm-10">
                                            <button type="button" onclick="updateUser()"  class="btn btn-primary">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-xxl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Familias</h4>
                            <div class="basic-form">
                                <form>
                                    <div class="form-group">
                                        @foreach($fam_asig as $asignada)
                                            @if($asignada['Asignado'] != 0)
                                                <div class="form-check mb-2">
                                                    <input onclick="setFamilia({{$asignada['Id']}})" type="checkbox" class="form-check-input" id="fam{{$asignada['Id']}}" value="{{$asignada['Id']}}" checked >
                                                    <label class="form-check-label" for="check1">{{$asignada['Nombre']}}</label>
                                                </div>
                                            @else
                                                <div class="form-check mb-2">
                                                    <input onclick="setFamilia({{$asignada['Id']}})" type="checkbox" class="form-check-input" id="fam{{$asignada['Id']}}" value="{{$asignada['Id']}}"  >
                                                    <label class="form-check-label" for="check1">{{$asignada['Nombre']}}</label>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h3 class="content-heading">Asignaciones</h3>

                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4 class="card-title">Empleados Asignados</h4>
                            <div class="mr-2 mb-2 mb-lg-0 pull-right">
                                <button onclick="quitAll()" class="btn btn-danger danger-alert-right-bottom">Quitar todos</button>
                            </div>
                            <div class="mr-2 mb-2 mb-lg-0 pull-right">
                                <a href="{{route('usuarios.addemp', $id)}}" class="btn btn-success danger-alert-right-bottom">Asignar</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="empleados" class="display" style="min-width: 845px">
                                    <thead>
                                    <tr>
                                        <th>Clave</th>
                                        <th>Nombre</th>
                                        <th>Puesto</th>
                                        <th>Razón Social</th>
                                        <th>Plaza</th>
                                        <th>Zona</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($empleados as $empleado)
                                        <tr>
                                            <td>{{$empleado->clave}}</td>
                                            <td>{{$empleado->nombre." ".$empleado->apepat." ".$empleado->apemat }}</td>
                                            <td>{{$empleado->puesto}}</td>
                                            <td>{{$empleado->razon}}</td>
                                            <td>{{$empleado->plaza}}</td>
                                            <td>{{$empleado->zona}}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Clave</th>
                                            <th>Nombre</th>
                                            <th>Puesto</th>
                                            <th>Razón Social</th>
                                            <th>Plaza</th>
                                            <th>Zona</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4 class="card-title">Sucursales Asignadas</h4>
                            <div class="mr-2 mb-2 mb-lg-0 pull-right">
                                <button onclick="quitAllSuc()" class="btn btn-danger danger-alert-right-bottom">Quitar todos</button>
                            </div>
                            <div class="mr-2 mb-2 mb-lg-0 pull-right">
                                <a  href="{{route('usuarios.addsuc', $id)}}"  class="btn btn-success danger-alert-right-bottom">Asignar</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="tiendas" class="display" style="min-width: 845px">
                                    <thead>
                                    <tr>
                                        <th>Clave</th>
                                        <th>Sucursal</th>
                                        <th>Razón Social</th>
                                        <th>Plaza</th>
                                        <th>Zona</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tiendas as $tienda)
                                        <tr>
                                            <td>{{$tienda->numsuc}}</td>
                                            <td>{{$tienda->nombre }}</td>
                                            <td>{{$tienda->razon}}</td>
                                            <td>{{$tienda->plaza}}</td>
                                            <td>{{$tienda->zona}}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Clave</th>
                                            <th>Sucursal</th>
                                            <th>Razón Social</th>
                                            <th>Plaza</th>
                                            <th>Zona</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>


                    </div>
                    <div class="mr-2 mb-2 mb-lg-0 pull-right">
                        <a href="{{route('usuarios.lista')}}" class="btn btn-primary primary-alert-center-top">Regresar</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!--**********************************
        Content body end
    ***********************************-->
@endsection
@section('css')
    <!-- JS Grid -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jsgrid/css/jsgrid.min.css')}}">
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jsgrid/css/jsgrid-theme.min.css')}}">
    <!-- Footable -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/footable/css/footable.bootstrap.min.css')}}">
    <!-- Bootgrid -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.css')}}">
    <!-- Datatable -->
    <link href="{{ asset('drora/assets/plugins/datatables/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <!-- Form step -->
    <link href="{{ asset('drora/assets/plugins/jquery-steps/css/jquery.steps.css')}}" rel="stylesheet">
    <!-- Dropify -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/dropify/dist/css/dropify.min.css')}}">
    <!-- Tagsinput -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css')}}">
    <!-- Switchery -->
    <link href="{{ asset('drora/assets/plugins/innoto-switchery/dist/switchery.min.css')}}" rel="stylesheet"/>
    <!-- Select 2 -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/select2/css/select2.min.css')}}">
    <!-- Touchspinner -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css')}}">
    <!-- Input mask -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jasny-bootstrap/dist/css/jasny-bootstrap.min.css')}}">
    <!-- x-editable -->
    <link href="{{ asset('drora/assets/plugins/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css')}}" rel="stylesheet">
    <!-- Summernote -->
    <link href="{{ asset('drora/assets/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <!-- Daterange picker -->
    <link href="{{ asset('drora/assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <!-- Clockpicker -->
    <link href="{{ asset('drora/assets/plugins/clockpicker/css/bootstrap-clockpicker.min.css')}}" rel="stylesheet">
    <!-- asColorpicker -->
    <link href="{{ asset('drora/assets/plugins/jquery-asColorPicker/css/asColorPicker.min.css')}}" rel="stylesheet">
    <!-- Material color picker -->
    <link href="{{ asset('drora/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
    <!-- Pick date -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/pickadate/themes/default.css')}}">
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/pickadate/themes/default.date.css')}}">

@endsection
@section('js')
    <script>
        var upd = '{{route('updateUser')}}';
        var updFam = '{{route('updateUsrFam')}}';
        var quit = '{{route('quitAll')}}';
        var quitSuc = '{{route('quitAllSuc')}}';
        var type = '{{$info->tipo_user}}';
        var emp = '{{$info->empleados_Id}}';
        var csrf = '{{ csrf_token() }}';
        var url_edit = '{{route('usuarios.edit', $id)}}';

        $( document ).ready(function()
        {

            setInfo();

        });
    </script>
    <!--Script de request-->
    <script src="{{ asset('scripts/usuarios.js')}}"></script>
    <!-- JS Grid -->
    <script src="{{ asset('drora/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/jsgrid/js/jsgrid.min.js')}}"></script>
    <!-- Footable -->
    <script src="{{ asset('drora/assets/plugins/footable/js/footable.min.js')}}"></script>
    <!-- Bootgrid -->
    <script src="{{ asset('drora/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.js')}}"></script>
    <!-- Datatable -->
    <script src="{{ asset('drora/assets/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
    <!-- JS Grid Init -->
    <script src="{{ asset('drora/main/js/plugins-init/jsgrid-init.js')}}"></script>
    <script src="{{ asset('drora/main/js/plugins-init/footable-init.js')}}"></script>
    <script src="{{ asset('drora/main/js/plugins-init/jquery.bootgrid-init.js')}}"></script>
    <script src="{{ asset('drora/main/js/plugins-init/datatables.init.js')}}"></script>

    /**para formularios**/
    <!-- Jquery Validation -->
    <script src="{{ asset('drora/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <!-- Form step -->
    <script src="{{ asset('drora/assets/plugins/jquery-steps/build/jquery.steps.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <!-- Form validate init -->
    <script src="{{ asset('drora/main/js/plugins-init/jquery.validate-init.js')}}"></script>
    <!-- Dropify -->
    <script src="{{ asset('drora/assets/plugins/dropify/dist/js/dropify.min.js')}}"></script>
    <!-- Typeahead -->
    <script src="{{ asset('drora/assets/plugins/typeahead.js/dist/typeahead.jquery.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/typeahead.js/dist/typeahead.bundle.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/typeahead.js/dist/bloodhound.min.js')}}"></script>
    <!-- Tagsinput -->
    <script src="{{ asset('drora/assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js')}}"></script>
    <!-- Switchery -->
    <script src="{{ asset('drora/assets/plugins/innoto-switchery/dist/switchery.min.js')}}"></script>
    <!-- Select 2 -->
    <script src="{{ asset('drora/assets/plugins/select2/js/select2.full.min.js')}}"></script>
    <!-- Touchspinner -->
    <script src="{{ asset('drora/assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js')}}"></script>
    <!-- Input mask -->
    <script src="{{ asset('drora/assets/plugins/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')}}"></script>
    <!-- x-editable -->
    <script src="{{ asset('drora/assets/plugins/moment/moment.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js')}}"></script>
    <!-- Summernote -->
    <script src="{{ asset('drora/assets/plugins/summernote/js/summernote.min.js')}}"></script>
    <!-- Daterangepicker -->
    <!-- momment js is must -->
    <!-- <script src="{{ asset('drora/assets/plugins/moment/moment.min.js')}}"></script> -->
    <script src="{{ asset('drora/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!-- clockpicker -->
    <script src="{{ asset('drora/assets/plugins/moment/moment.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js')}}"></script>
    <!-- asColorPicker -->
    <!-- momment js is must -->
    <!-- <script src="{{ asset('drora/assets/plugins/moment/moment.min.js')}}"></script> -->
    <script src="{{ asset('drora/assets/plugins/jquery-asColor/jquery-asColor.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/jquery-asGradient/jquery-asGradient.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/jquery-asColorPicker/js/jquery-asColorPicker.min.js')}}"></script>
    <!-- Material color picker -->
    <!-- momment js is must -->
    <!-- <script src="{{ asset('drora/assets/plugins/moment/moment.min.js')}}"></script> -->
    <script src="{{ asset('drora/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <!-- pickdate -->
    <script src="{{ asset('drora/assets/plugins/pickadate/picker.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/pickadate/picker.time.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/pickadate/picker.date.js')}}"></script>


    <!-- Form step init -->
    <script src="{{ asset('drora/main/js/plugins-init/jquery-steps-init.js')}}"></script>
    <!-- Dropify init -->
    <script src="{{ asset('drora/main/js/plugins-init/dropify-init.js')}}"></script>
    <!-- Typeahead init -->
    <script src="{{ asset('drora/main/js/plugins-init/typehead.js-init.js')}}"></script>
    <!-- Tagsinput init -->
    <script src="{{ asset('drora/main/js/plugins-init/bootstrap-tagsinput-init.js')}}"></script>
    <!-- Switchery init -->
    <script src="{{ asset('drora/main/js/plugins-init/switchery-init.js')}}"></script>
    <!-- Select 2 init -->
    <script src="{{ asset('drora/main/js/plugins-init/select2-init.js')}}"></script>
    <!-- Touchspinner init -->
    <script src="{{ asset('drora/main/js/plugins-init/bootstrap-touchpin-init.js')}}"></script>
    <!-- x-editable init -->
    <script src="{{ asset('drora/main/js/plugins-init/bootstrap-editable-init.js')}}"></script>
    <!-- Summernote init -->
    <script src="{{ asset('drora/main/js/plugins-init/summernote-init.js')}}"></script>
    <!-- Daterangepicker -->
    <script src="{{ asset('drora/main/js/plugins-init/bs-daterange-picker-init.js')}}"></script>
    <!-- Clockpicker init -->
    <script src="{{ asset('drora/main/js/plugins-init/clock-picker-init.js')}}"></script>
    <!-- asColorPicker init -->
    <script src="{{ asset('drora/main/js/plugins-init/jquery-asColorPicker.init.js')}}"></script>
    <!-- Material color picker init -->
    <script src="{{ asset('drora/main/js/plugins-init/material-date-picker-init.js')}}"></script>
    <!-- Pickdate -->
    <script src="{{ asset('drora/main/js/plugins-init/pickadate-init.js')}}"></script>
@endsection