<!--
 // **************************************************
 // ******* Name: drora
 // ******* Description: Bootstrap 4 Admin Dashboard
 // ******* Version: 1.0.0
 // ******* Released on 2019-02-08 15:41:24
 // ******* Support Email : quixlab.com@gmail.com
 // ******* Support Skype : sporsho9
 // ******* Author: Quixlab
 // ******* URL: https://quixlab.com
 // ******* Themeforest Profile : https://themeforest.net/user/quixlab
 // ******* License: ISC
 // ***************************************************
-->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>@yield('title_prefix')</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('drora/assets/images/favicon.png')}}">

    <link href="{{ asset('drora/main/css/style.css')}}" rel="stylesheet">
    @yield('css')
</head>

<body>

<!--*******************
    Preloader start
********************-->
<!--<div id="preloader">
    <div class="loader"></div>
</div>-->
<!--*******************
    Preloader end
********************-->
<!--**********************************
    Main wrapper start
***********************************-->
<div id="main-wrapper">
    @include('menus.nav_header')
    @include('menus.header')
    @include('menus.sidebar')
    @yield('body')
    <div class="clear"> </div>
    @include('menus.footer')

</div>
<!--**********************************
    Main wrapper end
***********************************-->

<!--**********************************
    Scripts
***********************************-->
<script src="{{ asset('drora/assets/plugins/common/common.min.js')}}"></script>
<script src="{{ asset('drora/main/js/custom.min.js')}}"></script>
<script src="{{ asset('drora/main/js/settings.js')}}"></script>
<script src="{{ asset('drora/main/js/quixnav.js')}}"></script>
<script src="{{ asset('drora/main/js/styleSwitcher.js')}}"></script>


<!--**********************************
    Sweet alert
***********************************-->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

@yield('js')
</body>

</html>