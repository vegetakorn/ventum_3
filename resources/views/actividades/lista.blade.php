@section('title_prefix')
    Ventum 3.0::Empleados
@endsection
@extends('home')
@section('body')
    <!--**********************************
            Content body start
        ***********************************-->
    <div class="content-body">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="breadcrumb-range-picker">
                    <span><i class="icon-calender"></i></span>
                    <span class="ml-1">{{date('d/m/Y')}}</span>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('actividades.lista')}}">Actividades de Usuario</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h3 class="content-heading">Administración de Actividades de Usuario</h3>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4 class="card-title">Filtro de Actividades</h4>
                        </div>
                        <div class="card-body">


                            <div class="basic-form">

                                <div class="input-group mb-3">
                                    <p class="mb-1">Busca supervisor</p>
                                    <select id="sup" onchange="getPlazas()"  class="custom-select" >
                                        @foreach($supervisores as $data)
                                            @if(Auth::user()->empleados_Id != $data->Id)
                                                <option value="{{$data->Id}}" >{{$data->nombre}} {{$data->apepat}}  {{$data->apemat}} </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group mb-3">
                                    <p class="mb-1">Estatus de la actividad</p>

                                </div>
                                <form>
                                    <div class="form-group">
                                        <div class="form-check mb-2">
                                            <input type="checkbox" class="form-check-input" id="boxOpen"  >
                                            <label class="form-check-label" for="check1">Abiertos</label>
                                        </div>
                                        <div class="form-check mb-2">
                                            <input type="checkbox" class="form-check-input" id="boxClosed"  >
                                            <label class="form-check-label" for="check1">Cerrados</label>
                                        </div>
                                    </div>
                                </form>
                                <div class="input-group mb-3">
                                    <p class="mb-1">Rango de fechas</p>

                                </div>
                                <input class="form-control input-daterange-datepicker" type="text"  name="daterange" id="date">
                                <div class="input-group mb-3">
                                    <p class="mb-1"></p>

                                </div>
                                <div class="mr-2 mb-2 mb-lg-0 pull-right">
                                    <button type="button" onclick="filtroActividades()" class="btn btn-primary primary-alert-center-top">Buscar</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="table_checklist" class="display" style="min-width: 845px">
                                    <thead>
                                    <tr>
                                        <th>Foto</th>
                                        <th>Actividad</th>
                                        <th>Descripcion</th>
                                        <th>Responsables</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Foto</td>
                                        <td>Actividad</td>
                                        <td>Descripcion</td>
                                        <td><table>
                                                <tr>
                                                    <td>Hector</td>
                                                    <td>20%</td>
                                                </tr>
                                                <tr>
                                                    <td>Hector</td>
                                                    <td>20%</td>
                                                </tr>
                                                <tr>
                                                    <td>Hector</td>
                                                    <td>20%</td>
                                                </tr>
                                                <tr>
                                                    <td>Hector</td>
                                                    <td>20%</td>
                                                </tr>
                                            </table></td>
                                        <td>Status</td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Foto</th>
                                        <th>Actividad</th>
                                        <th>Descripcion</th>
                                        <th>Responsables</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--**********************************
        Content body end
    ***********************************-->
@endsection
@section('css')
    <!-- JS Grid -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jsgrid/css/jsgrid.min.css')}}">
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jsgrid/css/jsgrid-theme.min.css')}}">
    <!-- Footable -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/footable/css/footable.bootstrap.min.css')}}">
    <!-- Bootgrid -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.css')}}">
    <!-- Datatable -->
    <link href="{{ asset('drora/assets/plugins/datatables/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <!-- Form step -->
    <link href="{{ asset('drora/assets/plugins/jquery-steps/css/jquery.steps.css')}}" rel="stylesheet">
    <!-- Dropify -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/dropify/dist/css/dropify.min.css')}}">
    <!-- Tagsinput -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css')}}">
    <!-- Switchery -->
    <link href="{{ asset('drora/assets/plugins/innoto-switchery/dist/switchery.min.css')}}" rel="stylesheet"/>
    <!-- Select 2 -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/select2/css/select2.min.css')}}">
    <!-- Touchspinner -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css')}}">
    <!-- Input mask -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jasny-bootstrap/dist/css/jasny-bootstrap.min.css')}}">
    <!-- x-editable -->
    <link href="{{ asset('drora/assets/plugins/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css')}}" rel="stylesheet">
    <!-- Summernote -->
    <link href="{{ asset('drora/assets/plugins/summernote/summernote.css')}}" rel="stylesheet">
    <!-- Daterange picker -->
    <link href="{{ asset('drora/assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <!-- Clockpicker -->
    <link href="{{ asset('drora/assets/plugins/clockpicker/css/bootstrap-clockpicker.min.css')}}" rel="stylesheet">
    <!-- asColorpicker -->
    <link href="{{ asset('drora/assets/plugins/jquery-asColorPicker/css/asColorPicker.min.css')}}" rel="stylesheet">
    <!-- Material color picker -->
    <link href="{{ asset('drora/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
    <!-- Pick date -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/pickadate/themes/default.css')}}">
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/pickadate/themes/default.date.css')}}">
@endsection
@section('js')

    <script>
        var url_act = '{{route('getActividadFiltro')}}';
        var csrf = '{{ csrf_token() }}';

        $( document ).ready(function()
        {

            $('.custom-select').select2({});
            $('#table_checklist').DataTable( {
                "responsive": true,
                "processing": true,
                destroy: true,
                "language": {
                    "lengthMenu": "Mostrando _MENU_ registros por página ",
                    "zeroRecords": "Sin registros encontrados",
                    "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "Sin regustros encontrados",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                }
            } );

            datePicker(30);


        });
    </script>
    <!--Script de request-->
    <script src="{{ asset('scripts/actividades.js')}}"></script>
    <script src="{{ asset('scripts/globales.js')}}"></script>
    <!-- JS Grid -->
    <script src="{{ asset('drora/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/jsgrid/js/jsgrid.min.js')}}"></script>
    <!-- Footable -->
    <script src="{{ asset('drora/assets/plugins/footable/js/footable.min.js')}}"></script>
    <!-- Bootgrid -->
    <script src="{{ asset('drora/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.js')}}"></script>
    <!-- Datatable -->
    <script src="{{ asset('drora/assets/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
    <!-- JS Grid Init -->
    <script src="{{ asset('drora/main/js/plugins-init/jsgrid-init.js')}}"></script>
    <script src="{{ asset('drora/main/js/plugins-init/footable-init.js')}}"></script>
    <script src="{{ asset('drora/main/js/plugins-init/jquery.bootgrid-init.js')}}"></script>
    <script src="{{ asset('drora/main/js/plugins-init/datatables.init.js')}}"></script>

    /**para formularios**/
    <!-- Jquery Validation -->
    <script src="{{ asset('drora/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <!-- Form step -->
    <script src="{{ asset('drora/assets/plugins/jquery-steps/build/jquery.steps.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <!-- Form validate init -->
    <script src="{{ asset('drora/main/js/plugins-init/jquery.validate-init.js')}}"></script>
    <!-- Dropify -->
    <script src="{{ asset('drora/assets/plugins/dropify/dist/js/dropify.min.js')}}"></script>
    <!-- Typeahead -->
    <script src="{{ asset('drora/assets/plugins/typeahead.js/dist/typeahead.jquery.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/typeahead.js/dist/typeahead.bundle.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/typeahead.js/dist/bloodhound.min.js')}}"></script>
    <!-- Tagsinput -->
    <script src="{{ asset('drora/assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js')}}"></script>
    <!-- Switchery -->
    <script src="{{ asset('drora/assets/plugins/innoto-switchery/dist/switchery.min.js')}}"></script>
    <!-- Select 2 -->
    <script src="{{ asset('drora/assets/plugins/select2/js/select2.full.min.js')}}"></script>
    <!-- Touchspinner -->
    <script src="{{ asset('drora/assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js')}}"></script>
    <!-- Input mask -->
    <script src="{{ asset('drora/assets/plugins/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')}}"></script>
    <!-- x-editable -->
    <script src="{{ asset('drora/assets/plugins/moment/moment.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js')}}"></script>
    <!-- Summernote -->
    <script src="{{ asset('drora/assets/plugins/summernote/js/summernote.min.js')}}"></script>
    <!-- Daterangepicker -->
    <!-- momment js is must -->
    <!-- <script src="{{ asset('drora/assets/plugins/moment/moment.min.js')}}"></script> -->
    <script src="{{ asset('drora/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!-- clockpicker -->
    <script src="{{ asset('drora/assets/plugins/moment/moment.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js')}}"></script>
    <!-- asColorPicker -->
    <!-- momment js is must -->
    <!-- <script src="{{ asset('drora/assets/plugins/moment/moment.min.js')}}"></script> -->
    <script src="{{ asset('drora/assets/plugins/jquery-asColor/jquery-asColor.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/jquery-asGradient/jquery-asGradient.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/jquery-asColorPicker/js/jquery-asColorPicker.min.js')}}"></script>
    <!-- Material color picker -->
    <!-- momment js is must -->
    <!-- <script src="{{ asset('drora/assets/plugins/moment/moment.min.js')}}"></script> -->
    <script src="{{ asset('drora/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <!-- pickdate -->
    <script src="{{ asset('drora/assets/plugins/pickadate/picker.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/pickadate/picker.time.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/pickadate/picker.date.js')}}"></script>


    <!-- Form step init -->
    <script src="{{ asset('drora/main/js/plugins-init/jquery-steps-init.js')}}"></script>
    <!-- Dropify init -->
    <script src="{{ asset('drora/main/js/plugins-init/dropify-init.js')}}"></script>
    <!-- Typeahead init -->
    <script src="{{ asset('drora/main/js/plugins-init/typehead.js-init.js')}}"></script>
    <!-- Tagsinput init -->
    <script src="{{ asset('drora/main/js/plugins-init/bootstrap-tagsinput-init.js')}}"></script>
    <!-- Switchery init -->
    <script src="{{ asset('drora/main/js/plugins-init/switchery-init.js')}}"></script>
    <!-- Select 2 init -->
    <script src="{{ asset('drora/main/js/plugins-init/select2-init.js')}}"></script>
    <!-- Touchspinner init -->
    <script src="{{ asset('drora/main/js/plugins-init/bootstrap-touchpin-init.js')}}"></script>
    <!-- x-editable init -->
    <script src="{{ asset('drora/main/js/plugins-init/bootstrap-editable-init.js')}}"></script>
    <!-- Summernote init -->
    <script src="{{ asset('drora/main/js/plugins-init/summernote-init.js')}}"></script>
    <!-- Daterangepicker -->
    <script src="{{ asset('drora/main/js/plugins-init/bs-daterange-picker-init.js')}}"></script>
    <!-- Clockpicker init -->
    <script src="{{ asset('drora/main/js/plugins-init/clock-picker-init.js')}}"></script>
    <!-- asColorPicker init -->
    <script src="{{ asset('drora/main/js/plugins-init/jquery-asColorPicker.init.js')}}"></script>
    <!-- Material color picker init -->
    <script src="{{ asset('drora/main/js/plugins-init/material-date-picker-init.js')}}"></script>
    <!-- Pickdate -->
    <script src="{{ asset('drora/main/js/plugins-init/pickadate-init.js')}}"></script>
@endsection