@section('title_prefix')
    Ventum 3.0::Editar Usuario
@endsection
@extends('home')
@section('body')
    <!--**********************************
            Content body start
        ***********************************-->
    <div class="content-body">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="breadcrumb-range-picker">
                    <span><i class="icon-calender"></i></span>
                    <span class="ml-1">{{date('d/m/Y')}}</span>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('panel.panel')}}">Configuración General</a></li>
                </ol>
            </div>
        </div>
        <!-- row -->
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <h3 class="content-heading mt-3">Configuración de Sistema</h3>
                </div>
                <div class="col-xl-6 col-xxl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Títulos</h4>
                            <div class="basic-form">
                                <form>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Razón Social</label>
                                        <div class="col-sm-10">
                                            <input class="form-control" type="text" value="" >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Plaza</label>
                                        <div class="col-sm-10">
                                            <input class="form-control" type="text" value="" >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Zona</label>
                                        <div class="col-sm-10">
                                            <input class="form-control" type="text" value="" >
                                        </div>
                                    </div>
                                    <div class="form-group row pull-right">
                                        <div class="col-sm-10 pull-right">
                                            <button type="button"  class="btn btn-primary pull-right">Guardar</button>
                                        </div>
                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-xxl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Validación de visita</h4>
                            <div class="basic-form">
                                <form>

                                    <fieldset class="form-group">
                                        <div class="row">
                                            <label class="col-form-label col-sm-2 pt-0">Selecciona</label>
                                            <div class="col-sm-10">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="gridRadios" value="0" >
                                                    <label class="form-check-label">
                                                        Sin validación al terminar la visita
                                                    </label>
                                                </div>

                                                <div class="form-check disabled">
                                                    <input class="form-check-input" type="radio" name="gridRadios" value="1" >
                                                    <label class="form-check-label">
                                                        Validar por contraseña de sucursal
                                                    </label>
                                                </div>
                                                <div class="form-check disabled">
                                                    <input class="form-check-input" type="radio" name="gridRadios" value="2" >
                                                    <label class="form-check-label">
                                                        Validar por escaneo de código
                                                    </label>
                                                </div>
                                                <div class="form-check disabled">
                                                    <input class="form-check-input" type="radio" name="gridRadios" value="3" >
                                                    <label class="form-check-label">
                                                        Validar por geolocalización
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-xxl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Ventas a Crédito</h4>
                            <div class="basic-form">
                                <form>

                                    <fieldset class="form-group">
                                        <div class="row">
                                            <label class="col-form-label col-sm-2 pt-0">Selecciona</label>
                                            <div class="col-sm-10">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="gridCred" value="0" >
                                                    <label class="form-check-label">
                                                      Subir ventas completas
                                                    </label>
                                                </div>

                                                <div class="form-check disabled">
                                                    <input class="form-check-input" type="radio" name="gridCred" value="1" >
                                                    <label class="form-check-label">
                                                       Subir ventas y ventas a crédito por separado
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-xxl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Formato de Reportes</h4>
                            <div class="basic-form">
                                <form>

                                    <fieldset class="form-group">
                                        <div class="row">
                                            <label class="col-form-label col-sm-2 pt-0">Selecciona</label>
                                            <div class="col-sm-10">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="gridFormat" value="0" >
                                                    <label class="form-check-label">
                                                       Reporte completo con fotografías
                                                    </label>
                                                </div>

                                                <div class="form-check disabled">
                                                    <input class="form-check-input" type="radio" name="gridFormat" value="1" >
                                                    <label class="form-check-label">
                                                        Reporte básico sin fotografías
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>


    <!--**********************************
        Content body end
    ***********************************-->
@endsection
@section('css')
    <!-- JS Grid -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jsgrid/css/jsgrid.min.css')}}">
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jsgrid/css/jsgrid-theme.min.css')}}">
    <!-- Footable -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/footable/css/footable.bootstrap.min.css')}}">
    <!-- Bootgrid -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.css')}}">
    <!-- Datatable -->
    <link href="{{ asset('drora/assets/plugins/datatables/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <!-- Form step -->
    <link href="{{ asset('drora/assets/plugins/jquery-steps/css/jquery.steps.css')}}" rel="stylesheet">
    <!-- Dropify -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/dropify/dist/css/dropify.min.css')}}">
    <!-- Tagsinput -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css')}}">
    <!-- Switchery -->
    <link href="{{ asset('drora/assets/plugins/innoto-switchery/dist/switchery.min.css')}}" rel="stylesheet"/>
    <!-- Select 2 -->
    <link rel="stylesheet" href="{{ asset('drora/assets/plugins/select2/css/select2.min.css')}}">


@endsection
@section('js')
    <script>
        var csrf = '{{ csrf_token() }}';
        $( document ).ready(function()
        {



        });
    </script>
    <!--Script de request-->
    <script src="{{ asset('scripts/usuarios.js')}}"></script>
    <!-- JS Grid -->
    <script src="{{ asset('drora/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/jsgrid/js/jsgrid.min.js')}}"></script>
    <!-- Footable -->
    <script src="{{ asset('drora/assets/plugins/footable/js/footable.min.js')}}"></script>
    <!-- Bootgrid -->
    <script src="{{ asset('drora/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.js')}}"></script>
    <!-- Datatable -->
    <script src="{{ asset('drora/assets/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
    <!-- JS Grid Init -->
    <script src="{{ asset('drora/main/js/plugins-init/jsgrid-init.js')}}"></script>
    <script src="{{ asset('drora/main/js/plugins-init/footable-init.js')}}"></script>
    <script src="{{ asset('drora/main/js/plugins-init/jquery.bootgrid-init.js')}}"></script>
    <script src="{{ asset('drora/main/js/plugins-init/datatables.init.js')}}"></script>


    <!-- Jquery Validation -->
    <script src="{{ asset('drora/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <!-- Form step -->
    <script src="{{ asset('drora/assets/plugins/jquery-steps/build/jquery.steps.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <!-- Form validate init -->
    <script src="{{ asset('drora/main/js/plugins-init/jquery.validate-init.js')}}"></script>
    <!-- Dropify -->
    <script src="{{ asset('drora/assets/plugins/dropify/dist/js/dropify.min.js')}}"></script>
    <!-- Typeahead -->
    <script src="{{ asset('drora/assets/plugins/typeahead.js/dist/typeahead.jquery.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/typeahead.js/dist/typeahead.bundle.min.js')}}"></script>
    <script src="{{ asset('drora/assets/plugins/typeahead.js/dist/bloodhound.min.js')}}"></script>
    <!-- Tagsinput -->
    <script src="{{ asset('drora/assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js')}}"></script>
    <!-- Switchery -->
    <script src="{{ asset('drora/assets/plugins/innoto-switchery/dist/switchery.min.js')}}"></script>
    <!-- Select 2 -->
    <script src="{{ asset('drora/assets/plugins/select2/js/select2.full.min.js')}}"></script>






    <!-- Form step init -->
    <script src="{{ asset('drora/main/js/plugins-init/jquery-steps-init.js')}}"></script>
    <!-- Dropify init -->
    <script src="{{ asset('drora/main/js/plugins-init/dropify-init.js')}}"></script>
    <!-- Typeahead init -->
    <script src="{{ asset('drora/main/js/plugins-init/typehead.js-init.js')}}"></script>
    <!-- Tagsinput init -->
    <script src="{{ asset('drora/main/js/plugins-init/bootstrap-tagsinput-init.js')}}"></script>
    <!-- Switchery init -->
    <script src="{{ asset('drora/main/js/plugins-init/switchery-init.js')}}"></script>
    <!-- Select 2 init -->
    <script src="{{ asset('drora/main/js/plugins-init/select2-init.js')}}"></script>


@endsection