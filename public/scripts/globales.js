
function getPlazas()
{
    if(countraz != 1)
    {
        razid = document.getElementById('razon').value;
    }

    $.ajax({
        method: 'POST',
        url: getPla,
        data: {idraz:  razid , _token:csrf }
    })
        .done(function(msg)
        {

            var text = "";

            if(msg['message'].length != 1)
            {
                text += ' <div class="input-group mb-3">\n' +
                    '                                            <p class="mb-1">Plazas</p>\n' +
                    '                                            <select id="plaza" onchange="getTiendas()"  class="plaza-select" >\n' +
                    '                                                <option value="0" selected="">Todas...</option>\n';

                for(var i = 0; i < msg['message'].length; i++)
                {
                    text += '             <option value="'+msg['message'][i]['Id']+'">'+msg['message'][i]['plaza']+'</option>\n' ;
                }

                text +=    '             </select>\n' +
                    '             </div>';
            }else
            {
                text +=    ' <input class="form-control" type="hidden" id="plaza" value="'+msg['message'][0]['Id']+'" >';
                getTiendas();
            }


            $('.plazas').html( text )
            $('.sucursales').html( ' <input class="form-control" type="hidden" id="sucursal" value="0" >' );
            $('.plaza-select').select2({});

            /** **/

        });
}

function getTiendas()
{
    var pla = document.getElementById('plaza').value;


    $.ajax({
        method: 'POST',
        url: getSuc,
        data: {idpla:  pla , _token:csrf }
    })
        .done(function(msg)
        {

            var text = "";


            text += ' <div class="input-group mb-3">\n' +
                '                                            <p class="mb-1">Sucursales</p>\n' +
                '                                            <select id="sucursal"   class="tienda-select" >\n' +
                '                                                <option value="0" selected="">Todas...</option>\n';
            for(var i = 0; i < msg['message'].length; i++)
            {
                text += '             <option value="'+msg['message'][i]['Id']+'">'+msg['message'][i]['nombre']+'</option>\n' ;
            }

            text +=    '             </select>\n' +
                '             </div>';

            $('.sucursales').html( text )
            $('.tienda-select').select2({});
            /** **/

        });
}

function filtroEmpleados()
{
    var razid = document.getElementById('razon').value;
    var plaid = document.getElementById('plaza').value;
    var sucid = document.getElementById('sucursal').value;
    var ptoid = document.getElementById('puesto').value;
    var id = document.getElementById('id').value;

    Swal.fire ({
        title: 'Buscando ...',
        onBeforeOpen: () => {
            Swal.showLoading ()
        }
    })

    $.ajax({
        method: 'POST',
        url: getFiltroEmp,
        data: {id: id, razid:  razid, plaid:plaid, sucid:sucid, ptoid:ptoid , _token:csrf }
    }).done(function(msg)
        {
            var dataSet = [];
            console.log(msg['message']);
            for(var i = 0; i < msg['message'].length; i++)
            {
                dataSet.push({
                    "Id" : msg['message'][i]['Id'],
                    "clave" : msg['message'][i]['Clave'],
                    'nombre': msg['message'][i]['Nombre'],
                    'puesto': msg['message'][i]['Puesto'],
                    'razon'  : msg['message'][i]['Razon'],
                    'plaza'  : msg['message'][i]['Plaza'],
                    'zona'  : msg['message'][i]['Zona']
                });
            }

            var table = $('#empleados').DataTable( {
                "responsive": true,
                "processing": true,
                destroy: true,
                data: dataSet,
                columns: [
                    { data: null, title: "Asignar",
                        sortable: false,
                        "render": function ( data, type, full, meta ) {
                            //Boton para mostrar información de asociado
                            botones = '' +


                           '<div class="input-group-prepend">\n' +
                           '                                                    <div class="input-group-text">\n' +
                           '                                                        <input onclick="asignaSingleEmp('+full.Id+')" id="emp'+full.Id+'" type="checkbox">\n' +
                           '                                                    </div>\n' +
                           '                                                </div>';

                            return botones;
                        }

                    },
                    { data: 'clave', title: "Clave" },
                    { data: 'nombre', title: "Nombre" },
                    { data: 'puesto', title: "Puesto" },
                    { data: 'razon', title: "Razón" },
                    { data: 'plaza', title: "Plaza" },
                    { data: 'zona', title: "Zona" },


                ],
                columnDefs: [
                    {
                        "targets": [ 0 ],

                        "visible":true,
                        "searchable": false
                    }
                ],

                "language": {
                    "lengthMenu": "Mostrando _MENU_ registros por página ",
                    "zeroRecords": "Sin registros encontrados",
                    "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "Sin regustros encontrados",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                }
            } );

            Swal.close();

        });
}

function filtroSucursales()
{
    var razid = document.getElementById('razon').value;
    var plaid = document.getElementById('plaza').value;
    var sucid = document.getElementById('sucursal').value;
    var id = document.getElementById('id').value;

    Swal.fire ({
        title: 'Buscando ...',
        onBeforeOpen: () => {
            Swal.showLoading ()
        }
    })

    $.ajax({
        method: 'POST',
        url: getFiltroSuc,
        data: {id: id, razid:  razid, plaid:plaid, sucid:sucid, _token:csrf }
    }).done(function(msg)
    {
        var dataSet = [];
        console.log(msg['message']);
        for(var i = 0; i < msg['message'].length; i++)
        {
            dataSet.push({
                "Id" : msg['message'][i]['Id'],
                "clave" : msg['message'][i]['Clave'],
                'nombre': msg['message'][i]['Nombre'],
                'razon'  : msg['message'][i]['Razon'],
                'plaza'  : msg['message'][i]['Plaza'],
                'zona'  : msg['message'][i]['Zona']
            });
        }

        var table = $('#sucursales').DataTable( {
            "responsive": true,
            "processing": true,
            destroy: true,
            data: dataSet,
            columns: [
                { data: null, title: "Asignar",
                    sortable: false,
                    "render": function ( data, type, full, meta ) {
                        //Boton para mostrar información de asociado
                        botones = '' +


                            '<div class="input-group-prepend">\n' +
                            '                                                    <div class="input-group-text">\n' +
                            '                                                        <input onclick="asignaSingleSuc('+full.Id+')" id="suc'+full.Id+'" type="checkbox">\n' +
                            '                                                    </div>\n' +
                            '                                                </div>';
                        return botones;
                    }

                },
                { data: 'clave', title: "Clave" },
                { data: 'nombre', title: "Nombre" },
                { data: 'razon', title: "Razón" },
                { data: 'plaza', title: "Plaza" },
                { data: 'zona', title: "Zona" },
            ],
            columnDefs: [
                {
                    "targets": [ 0 ],
                    "visible":true,
                    "searchable": false
                }
            ],

            "language": {
                "lengthMenu": "Mostrando _MENU_ registros por página ",
                "zeroRecords": "Sin registros encontrados",
                "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                "infoEmpty": "Sin regustros encontrados",
                "infoFiltered": "(filtrados de _MAX_ registros totales)",
                "search": "Buscar:",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            }
        } );

        Swal.close();

    });

}


function datePicker(dias)
{

    //Date range picker
    $('#date').daterangepicker({
        "startDate": moment().add(-dias, 'day'),
        "endDate": moment().add(1, 'day'),
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Aplicar",
            "cancelLabel": "Cancelar",
            "fromLabel": "De",
            "toLabel": "a",
            "customRangeLabel": "Custom",
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "firstDay": 1
        }
    })
}

