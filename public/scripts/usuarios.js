function updateUser()
{


    var pass = document.getElementById('inputPass').value;
    var id = document.getElementById('id').value;
    var type = $("input[name='gridRadios']:checked"). val();

    $.ajax({
         method: 'POST',
         url: upd,
         data: {id:  id, pass: pass, type: type , _token:csrf }
     })
         .done(function(msg){

             Swal.fire(
                 '¡Bien!',
                 'Actualización Exitosa',
                 'success'
             )

         });


}

function setInfo()
{
    setRadio();

    $('#empleados').DataTable( {
        "responsive": true,
        "processing": true,
        destroy: true,
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página ",
            "zeroRecords": "Sin registros encontrados",
            "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
            "infoEmpty": "Sin regustros encontrados",
            "infoFiltered": "(filtrados de _MAX_ registros totales)",
            "search": "Buscar:",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            }
        }
    } );

    $('#tiendas').DataTable( {
        "responsive": true,
        "processing": true,
        destroy: true,
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página ",
            "zeroRecords": "Sin registros encontrados",
            "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
            "infoEmpty": "Sin regustros encontrados",
            "infoFiltered": "(filtrados de _MAX_ registros totales)",
            "search": "Buscar:",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            }
        }
    } );
}



function setRadio()
{
    var radios = $('input:radio[name=gridRadios]');
    radios.filter('[value='+type+']').prop('checked', true);
}

function setFamilia(fam)
{
    var chk = '#fam'+fam;
    var id = document.getElementById('id').value;

    //se asigna familia
    $.ajax({
        method: 'POST',
        url: updFam,
        data: {id:  id, fam: fam, flag:$(chk).is(':checked')  , _token:csrf }
    })
        .done(function(msg){

            console.log(msg['message']);

           /* Swal.fire(
                '¡Bien!',
                msg['message'],
                'success'
            )*/

        });

}

function setActivo(flag, id)
{

    if(flag == 0)
    {
        var msg = "activar";
    }else
    {
        var msg = "desactivar";
    }
    Swal.fire({
        title: '¿Realmente deseas '+msg+' a este usuario?',
        text: "Aceptar para continuar",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Si, Continuar!',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {

            //se asigna familia
            $.ajax({
                method: 'POST',
                url: urlAct,
                data: {id:  id, flag:flag  , _token:csrf }
            })
                .done(function(msg){


                    window.location.href = url_lista;
                });
        }
    })
}

function asignaSingleEmp(id_emp)
{
    var emp = '#emp'+id_emp;
    var id = document.getElementById('id').value;

    //se asigna familia
    $.ajax({
        method: 'POST',
        url: asigEmp,
        data: {id:  id, id_emp: id_emp, flag:$(emp).is(':checked')  , _token:csrf }
    })
        .done(function(msg){

            console.log(msg['message']);

        });
}

function asignaSingleSuc(id_suc)
{
    var suc = '#suc'+id_suc;
    var id = document.getElementById('id').value;

    //se asigna familia
    $.ajax({
        method: 'POST',
        url: asigSuc,
        data: {id:  id, id_suc: id_suc, flag:$(suc).is(':checked')  , _token:csrf }
    })
        .done(function(msg){

            console.log(msg['message']);

        });
}


function quitAll()
{
    var id = document.getElementById('id').value;



    Swal.fire({
        title: '¿Quieres eliminar todos sus empleados?',
        text: "¡Una vez borrados no podras revertir esta acción!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Deseo continuar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value)
        {
            //se asigna familia
            $.ajax({
                method: 'POST',
                url: quit,
                data: {id:  id , _token:csrf }
            })
                .done(function(msg){

                    window.location.href = url_edit;

                });

        }
    })
}


function quitAllSuc()
{
    var id = document.getElementById('id').value;



    Swal.fire({
        title: '¿Quieres eliminar todas sus sucursales?',
        text: "¡Una vez borrados no podras revertir esta acción!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Deseo continuar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value)
        {
            //se asigna familia
            $.ajax({
                method: 'POST',
                url: quitSuc,
                data: {id:  id , _token:csrf }
            })
                .done(function(msg){

                    window.location.href = url_edit;

                });

        }
    })
}

function agregarFiltro()
{
    var razid = document.getElementById('razon').value;
    var plaid = document.getElementById('plaza').value;
    var ptoid = document.getElementById('puesto').value;
    var id = document.getElementById('id').value;

    Swal.fire ({
        title: 'Asignando ...',
        onBeforeOpen: () => {
            Swal.showLoading ()
        }
    })

    $.ajax({
        method: 'POST',
        url: addFiltroEmp,
        data: {id: id, razid:  razid, plaid:plaid, ptoid:ptoid , _token:csrf }
    }).done(function(msg)
    {
        Swal.close();
        filtroEmpleados();
        //window.location.href = url_edit;
    });
}

function agregarFiltroSuc()
{
    var razid = document.getElementById('razon').value;
    var plaid = document.getElementById('plaza').value;
    var id = document.getElementById('id').value;

    Swal.fire ({
        title: 'Asignando ...',
        onBeforeOpen: () => {
            Swal.showLoading ()
        }
    })

    $.ajax({
        method: 'POST',
        url: addFiltroSuc,
        data: {id: id, razid:  razid, plaid:plaid, _token:csrf }
    }).done(function(msg)
    {
        Swal.close();
        filtroSucursales();
        //window.location.href = url_edit;
    });


}